package com.clotl.config;

import com.clotl.entity.OrderStatus;
import com.clotl.jackson.CustomOrderStatusDeserializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class JacksonConfig {
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.deserializerByType(OrderStatus.class, new CustomOrderStatusDeserializer());
        return builder;
    }
}
