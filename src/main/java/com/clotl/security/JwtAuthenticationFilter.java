package com.clotl.security;

import com.clotl.exception.ErrorMessage;
import com.clotl.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private AccountService accountService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String jwt = getJwtFromRequest(request);
        RequestWrapper requestWrapper = new RequestWrapper(request);
        ObjectMapper mapper = new ObjectMapper();
        if (jwt != null) {
            try{
                String userName = tokenProvider.getUserNameFromToken(jwt);
                if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = accountService.loadUserByUsername(userName);
                    if (tokenProvider.validateJwtToken(jwt, userDetails)) {
                        UsernamePasswordAuthenticationToken
                                authenticationToken = new UsernamePasswordAuthenticationToken(
                                userDetails, null,
                                userDetails.getAuthorities());
                        authenticationToken.setDetails(new
                                WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }
            }catch (JwtException | UsernameNotFoundException e){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.setContentType("application/json");
                response.getWriter().write(mapper.writeValueAsString(new ErrorMessage(new Date(),e.getMessage(),"")));
                return;
            }
        }
        filterChain.doFilter(requestWrapper,response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    private static class RequestWrapper extends HttpServletRequestWrapper {
        private byte[] rawData;
        private final HttpServletRequest request;
        private final CustomServletInputStream  servletStream;

        public RequestWrapper(HttpServletRequest request) {
            super(request);
            this.request = request;
            this.servletStream = new CustomServletInputStream();
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            if (rawData == null) {
                rawData = StreamUtils.copyToByteArray(this.request.getInputStream());
                servletStream.inputStream = new ByteArrayInputStream(rawData);
            }
            return servletStream;
        }

        @Override
        public BufferedReader getReader() throws IOException {
            if (rawData == null) {
                rawData = StreamUtils.copyToByteArray(this.request.getInputStream());
                servletStream.inputStream = new ByteArrayInputStream(rawData);
            }
            return new BufferedReader(new InputStreamReader(servletStream));
        }

        public void modifiedInputStream(byte[] newRawData) {
            rawData = newRawData;
            servletStream.inputStream = new ByteArrayInputStream(newRawData);
        }

    }

    private static class CustomServletInputStream extends ServletInputStream {

        private InputStream inputStream;

        public CustomServletInputStream() {
        }

        public CustomServletInputStream(byte[] contents) {
            this.inputStream = new ByteArrayInputStream(contents);
        }

        @Override
        public int read() throws IOException {
            return this.inputStream.read();
        }

        @Override
        public boolean isFinished() {
            try {
                return inputStream.available() == 0;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener listener) {
        }
    }
}
