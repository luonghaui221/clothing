package com.clotl.mapper;

import com.clotl.DTO.AccountDTO;
import com.clotl.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountDTO toAccountDTO(Account acc);
    @Mapping(target = "birthday",dateFormat = "yyyy/MM/dd")
    Account toAccount(AccountDTO accountDTO);

    List<AccountDTO> toListAccountDTO(List<Account> accounts);
}
