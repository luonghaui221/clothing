package com.clotl.mapper;

import com.clotl.DTO.OrderDTO;
import com.clotl.DTO.OrderItemDTO;
import com.clotl.entity.Order;
import com.clotl.entity.OrderItem;
import com.clotl.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring",uses = {ProductMapper.class})
public interface OrderMapper {
    @Mapping(target = "orderItems",source = "orderItems", qualifiedByName = "toOrderItemDTOs")
    @Mapping(target = "userId",source = "account.id")
    @Mapping(target = "createBy",source = "account.name")
    OrderDTO toDTO(Order order);
    @Mapping(target = "orderItems",ignore = true)
    Order toOrder(OrderDTO orderDTO);

    @Mapping(target = "orderItems",source = "orderItems", qualifiedByName = "toOrderItemDTOs")
    List<OrderDTO> toListOrderDTO(List<Order> orders);


    @Named("toOrderItemDTOs")
    default Set<OrderItemDTO> toOrderItemDTOs(Set<OrderItem> orderItems){
        Set<OrderItemDTO> list = new HashSet<>();
        if(orderItems == null) return list;
        for(OrderItem oi : orderItems){
            Product p = oi.getVariants().getProduct();
            list.add(new OrderItemDTO(
                    oi.getVariants().getId(),
                    p.getName(),
                    oi.getVariants().getName(),
                    p.getPrice(),
                    p.getSalePrice(),
                    p.getSaleTo(),
                    oi.getVariants().getThumbnail(),
                    oi.getQuantity(),
                    oi.getSize()));
        }
        return list;
    }

}
