package com.clotl.mapper;

import com.clotl.DTO.ProductDTO;
import com.clotl.DTO.ProductSlimDTO;
import com.clotl.entity.Product;
import com.clotl.entity.Variants;
import com.clotl.repository.CustomRepository;
import com.clotl.repository.SizeRepository;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",uses = {VartiantsMapper.class,CategoryMapper.class})
public interface ProductMapper {
    @Mapping(target = "reviews",expression = "java(product.getReviews() != null ? product.getReviews().size() : 0)")
    ProductDTO ToProductDTO(Product product);

    @Mapping(ignore = true,target = "id")
    @Mapping(ignore = true,target = "createAt")
    @Mapping(ignore = true,target = "updateAt")
    @Mapping(ignore = true,target = "categories")
    @Mapping(ignore = true,target = "tags")
    @Mapping(ignore = true,target = "reviews")
    @Mapping(ignore = true,target = "collections")
    @Mapping(target = "variants",source = "variants",qualifiedByName = "toVariants")
    Product ToProduct(ProductDTO productDTO, @Context SizeRepository sizeRepository);

    @Mapping(ignore = true,target = "id")
    @Mapping(ignore = true,target = "createAt")
    @Mapping(ignore = true,target = "updateAt")
    @Mapping(ignore = true,target = "variants")
    @Mapping(ignore = true,target = "categories")
    @Mapping(ignore = true,target = "tags")
    @Mapping(ignore = true,target = "reviews")
    @Mapping(ignore = true,target = "collections")
    void UpdateExisting(ProductDTO ProductDTO, @MappingTarget Product product);
    default ProductSlimDTO toProductSlimDTO(Product product){
        if ( product == null ) {
            return null;
        }

        ProductSlimDTO productSlimDTO = new ProductSlimDTO();

        productSlimDTO.setId( product.getId() );
        productSlimDTO.setName( product.getName() );
        productSlimDTO.setPrice( product.getPrice() );
        productSlimDTO.setSalePrice( product.getSalePrice() );
        productSlimDTO.setDescription( product.getDescription() );
        productSlimDTO.setSaleTo( product.getSaleTo() );
        List<String> images = product.getVariants().stream()
                .map(v ->
                    StringUtils.hasText(v.getThumbnail()) ?
                            v.getThumbnail() :
                            "default-placeholder.png"
                )
                .collect(Collectors.toList());
        productSlimDTO.setImgs(images);

        return productSlimDTO;
    }
    default List<ProductSlimDTO> toListProductSlimDTO(List<Product> product){
        if ( product == null ) {
            return null;
        }

        List<ProductSlimDTO> productSlimDTO = new ArrayList<>();
        for(Product p : product){
            productSlimDTO.add(toProductSlimDTO(p));
        }
        return productSlimDTO;
    }
}
