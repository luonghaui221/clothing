package com.clotl.mapper;

import com.clotl.DTO.CategoryDTO;
import com.clotl.DTO.CategoryTreeData;
import com.clotl.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    @Mapping(source = "category.id",target = "categoryId")
    @Mapping(source = "category.name",target = "categoryName")
    @Mapping(ignore = true, target = "parent")
    CategoryDTO ToCategoryDto(Category category);

    @Mapping(source = "category.id",target = "key")
    @Mapping(source = "category.id",target = "value")
    @Mapping(source = "category.name",target = "title")
    CategoryTreeData ToCategoryTreeData(Category category);

    @Mapping(source = "dto.categoryId",target = "id")
    @Mapping(source = "dto.categoryName",target = "name")
    Category ToCategory(CategoryDTO dto);

    List<CategoryDTO> toListCategoryDTO(List<Category> categories);
    List<CategoryTreeData> toListCategoryTreeData(List<Category> categories);
}
