package com.clotl.mapper;

import com.clotl.DTO.CartItemDTO;
import com.clotl.entity.CartItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CartItemMapper {
    CartItem toCartItem(CartItemDTO cartItemDTO);
    @Mapping(target = "variantsId", source = "cartItem.id.variantsId")
    CartItemDTO toCartItemDTO(CartItem cartItem);

    default List<CartItemDTO> toListCartItemDTO(List<CartItem> cartItems){
        List<CartItemDTO> list = new ArrayList<>();
        if(cartItems == null) return list;
        cartItems.forEach(c->list.add(toCartItemDTO(c)));
        return list;
    }
}
