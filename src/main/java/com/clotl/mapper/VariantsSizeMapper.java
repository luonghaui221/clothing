package com.clotl.mapper;

import com.clotl.DTO.VariantsSizeDTO;
import com.clotl.entity.Size;
import com.clotl.entity.VariantsSize;
import com.clotl.exception.DoesNotExistException;
import com.clotl.repository.CustomRepository;
import com.clotl.repository.SizeRepository;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface VariantsSizeMapper{
    @Mapping(target = "sizeCode",source = "variants.size.code")
    VariantsSizeDTO ToVariantsSizeDTO(VariantsSize variants);

    @Named("toVariantSize")
    @Mapping(target = "size", source = "sizeCode", qualifiedByName = "idToSize")
    VariantsSize ToVariantsSize(VariantsSizeDTO variantsDTO, @Context SizeRepository sizeRepository);

    @Named("idToSize")
    default Size idToSize(String code, @Context SizeRepository sizeRepository){
        return sizeRepository.findByCode(code)
                .orElseThrow(()->new DoesNotExistException("size code does not exists"));
    }
}
