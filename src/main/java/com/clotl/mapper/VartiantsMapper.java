package com.clotl.mapper;

import com.clotl.DTO.ProductDTO;
import com.clotl.DTO.VariantsDTO;
import com.clotl.DTO.VariantsSizeDTO;
import com.clotl.entity.*;
import com.clotl.repository.CustomRepository;
import com.clotl.repository.SizeRepository;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",uses = {VariantsSizeMapper.class})
public interface VartiantsMapper {

    VariantsDTO ToVariantsDTO(Variants variants);

    @Named("toVariants")
    @Mapping(target = "sizes",source = "sizes",qualifiedByName = "toVariantSize")
    Variants ToVariants(VariantsDTO variantsDTO, @Context SizeRepository sizeRepository);

    List<VariantsSize> convertVariantsSize(List<VariantsSizeDTO> variantsSizeDTOS);

    @Mapping(target = "sizes",source = "sizes",qualifiedByName = "toVariantSize")
    default List<Variants> convert(List<VariantsDTO> variantsDTOS, @Context SizeRepository sizeRepository){
        if ( variantsDTOS == null ) {
            return null;
        }

        List<Variants> list = new ArrayList<>();
        for ( VariantsDTO variantsDTO : variantsDTOS ) {
            list.add( ToVariants( variantsDTO, sizeRepository ) );
        }

        return list;
    }
}
