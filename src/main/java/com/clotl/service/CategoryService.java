package com.clotl.service;

import com.clotl.DTO.CategoryDTO;
import com.clotl.DTO.CategoryTreeData;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Category;

import java.util.List;

public interface CategoryService {
    List<CategoryTreeData> getAllCategory();
    ResponsePagination getListCategory(Integer page, Integer pageSize);
    CategoryDTO getCategoryById(Long id);
    CategoryDTO addCategories(CategoryDTO category);
    CategoryDTO updateCategories(CategoryDTO category);
    void deleteCategories(Long id);
}
