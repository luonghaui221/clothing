package com.clotl.service;

import com.clotl.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


public interface ImageService {
    List<Image> findAllByVariantID(String id);
    byte[] findByName(String name) throws IOException;
    List<String> saveImages(MultipartFile[] files) throws IOException;
}
