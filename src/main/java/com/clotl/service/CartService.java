package com.clotl.service;

import com.clotl.DTO.CartItemDTO;
import com.clotl.DTO.CartVariantsInfo;
import com.clotl.entity.CartItem;

import java.util.List;

public interface CartService {
    List<CartVariantsInfo> getListCart(String userId);
    CartItemDTO addToCart(CartItemDTO cartItemDTO);
    List<CartItemDTO> addAllToCart(List<CartItemDTO> cartItemDTO);
    void removeFromCart(CartItemDTO cartItemDTO);
    CartItemDTO updateCart(CartItemDTO cartItemDTO);
}
