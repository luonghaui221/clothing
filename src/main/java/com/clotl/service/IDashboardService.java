package com.clotl.service;

import com.clotl.DTO.ResponsePagination;

import java.util.List;

public interface IDashboardService {
    double getRevenue(Integer day, Integer month, Integer year);
    ResponsePagination getProductOutOfStock(String name, Double minPrice, Double maxPrice, List<Long> categories, List<String> sizes,
                                            Integer page, Integer pageSize, String orderBy, String orderDirection);
}
