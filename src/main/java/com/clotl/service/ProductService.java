package com.clotl.service;

import com.clotl.DTO.ProductDTO;
import com.clotl.DTO.ProductSlimDTO;
import com.clotl.DTO.ResponsePagination;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    ResponsePagination getAll(String name, Double minPrice, Double maxPrice, List<Long> categories, List<String> sizes,
                              Integer page, Integer pageSize, String orderBy, String orderDirection);
    List<ProductSlimDTO> getAllInIds(List<Long> ids);
    ProductDTO save(ProductDTO toUpdate);
    ProductDTO update(ProductDTO product);
    void delete(Long id);
    void forceDelete(Long id);
    ProductDTO getById(Long id);
}
