package com.clotl.service;

import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.City;
import com.clotl.entity.Commune;
import com.clotl.entity.District;

import java.util.List;

public interface AddressService {
    ResponsePagination getAllCity(String code,String name, Integer page, Integer pageSize);
    ResponsePagination getAllDistrict(String code,String name, String cityCode, Integer page, Integer pageSize);
    ResponsePagination getAllCommune(String code,String name,String cityCode, String districtCode, Integer page, Integer pageSize);
    City getCityById(String id);
    District getDistrictById(String id);
    Commune getCommuneById(String id);
}
