package com.clotl.service;

import com.clotl.DTO.ResponseObject;


public interface RecommendService {
    ResponseObject getRecommendations(String id,int kNearest);
}
