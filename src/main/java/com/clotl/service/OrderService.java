package com.clotl.service;

import com.clotl.DTO.OrderDTO;
import com.clotl.DTO.OrderInfo;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.OrderStatus;

import java.util.Date;
import java.util.List;

public interface OrderService {
    ResponsePagination getListOrder(String email,String keywords, List<String> statuses,
                                    String phoneNumber, Date fromDate, Date toDate,
                                    Integer page, Integer pageSize,
                                    String orderBy, String orderDirection);
    OrderDTO getOderById(String id);
    OrderDTO create(OrderDTO orderDTO);
    OrderDTO update(OrderInfo orderDTO);
    void confirm(String id);
    void cancel(String id);
}
