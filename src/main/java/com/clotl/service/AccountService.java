package com.clotl.service;

import com.clotl.DTO.AccountDTO;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Account;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Date;
import java.util.List;

public interface AccountService {
    Account findById(String id);
    AccountDTO getById(String id);
    Account findByUserEmailAndPassword(String email,String password);
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    AccountDTO resgisterAccount(AccountDTO accountDTO);
    AccountDTO createAccount(AccountDTO accountDTO);
    AccountDTO updateAccount(AccountDTO accountDTO);
    void deleteAccount(String id);
    ResponsePagination getListAccount(String email, String keywords, List<String> statuses, String phoneNumber, Integer page, Integer pageSize, String orderBy, String orderDirection);
}
