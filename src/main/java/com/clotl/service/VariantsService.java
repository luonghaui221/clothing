package com.clotl.service;

import com.clotl.entity.Variants;
import java.util.List;
public interface VariantsService {
    List<Variants> saveAll(List<Variants> variants);
}
