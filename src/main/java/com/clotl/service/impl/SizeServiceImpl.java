package com.clotl.service.impl;

import com.clotl.entity.Size;
import com.clotl.mapper.SizeIdMapper;
import com.clotl.mapper.SizeIdToEntity;
import com.clotl.repository.SizeRepository;
import com.clotl.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SizeServiceImpl implements SizeService {

    @Autowired
    private SizeRepository sizeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Size> findAll() {
        List<Size> sizes = sizeRepository.findAll();
        sizes.forEach(Size::clearVariant);
        return sizes;
    }

    @Override
    @Transactional
    public Size addSize(Size size) {
        return sizeRepository.save(size);
    }

    @Override
    @Transactional
    public Size updateSize(Size size) {
        Size current = sizeRepository.findById(size.getCode()).orElseThrow();
        current.setName(size.getName());
        return current;
    }

    @Override
    @Transactional
    public void deleteSize(String sizeCode) {
        Size current = sizeRepository.findById(sizeCode).orElseThrow();
        sizeRepository.delete(current);
    }

    @Override
    public Size findById(String id) {
        return sizeRepository.findById(id).orElse(null);
    }
}
