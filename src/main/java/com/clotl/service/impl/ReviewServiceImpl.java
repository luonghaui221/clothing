package com.clotl.service.impl;

import com.clotl.DTO.ResponseObject;
import com.clotl.DTO.ResponsePagination;
import com.clotl.DTO.ReviewDTO;
import com.clotl.entity.Account;
import com.clotl.entity.UserReviewId;
import com.clotl.entity.UserReviews;
import com.clotl.exception.DoesNotExistException;
import com.clotl.repository.AccountRepository;
import com.clotl.repository.OrderRepository;
import com.clotl.repository.ProductRepository;
import com.clotl.repository.ReviewRepository;
import com.clotl.security.CustomUserDetails;
import com.clotl.service.ReviewService;
import com.clotl.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    AccountRepository accountRepository;

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getReviewOfProduct(Long id,Integer page, Integer pageSize) {
        Page<UserReviews> pageResult = reviewRepository.getAllByProductId(id, PageRequest.of(page-1,pageSize));
        List<ReviewDTO> data = pageResult.getContent().stream().map(x -> new ReviewDTO(
                x.getContent(),
                x.getId().getProductId(),
                x.getId().getUserId(),
                x.getAccount().getName(),
                x.getCreateAt())).collect(Collectors.toList());
        return new ResponsePagination(
                data,
                page,
                pageResult.getTotalPages(),
                (int) pageResult.getTotalElements(),
                pageResult.getSize()
        );
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public ResponseObject createOrUpdateReview(ReviewDTO reviewDTO) {
        CustomUserDetails user = Optional.ofNullable(Utils.getCurrentUser())
                .orElseThrow(()->new BadCredentialsException("Unauthenticated"));
        Account account = accountRepository.findAccountByEmail(user.getUsername())
                .orElseThrow(()->new BadCredentialsException("Unauthenticated"));
        boolean isBought = orderRepository.isBought(reviewDTO.getProductId(),user.getUsername());
        if(isBought){
            UserReviews ur = reviewRepository.findByUserIdAndProductId(account.getId(),reviewDTO.getProductId()).orElse(null);
            if(ur != null){
                ur.setContent(reviewDTO.getContent());
            }else{
                ur = new UserReviews();
                ur.setAccount(account);
                ur.setProduct(productRepository.findById(reviewDTO.getProductId())
                        .orElseThrow(()->new DoesNotExistException("Product does not exist")));
                ur.setContent(reviewDTO.getContent());
                ur.setId(new UserReviewId(reviewDTO.getProductId(),account.getId()));
            }
            reviewRepository.save(ur);
            return new ResponseObject(HttpStatus.OK.value(),"created",null);
        }
        throw new RuntimeException("Can only review on purchased products");
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public ResponseObject deleteReview(Long productId) {
        CustomUserDetails user = Optional.ofNullable(Utils.getCurrentUser())
                .orElseThrow(()->new BadCredentialsException("Unauthenticated"));
        Account account = accountRepository.findAccountByEmail(user.getUsername())
                .orElseThrow(()->new BadCredentialsException("Unauthenticated"));
        UserReviews ur = reviewRepository.findByUserIdAndProductId(account.getId(),productId)
                .orElseThrow(()->new DoesNotExistException("Review does not exist"));
        reviewRepository.delete(ur);
        return new ResponseObject(HttpStatus.OK.value(),"deleted",null);
    }
}
