package com.clotl.service.impl;

import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.City;
import com.clotl.entity.Commune;
import com.clotl.entity.District;
import com.clotl.entity.Product;
import com.clotl.repository.CityRepository;
import com.clotl.repository.CommuneRepository;
import com.clotl.repository.DistrictRepository;
import com.clotl.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    CityRepository cityRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    CommuneRepository communeRepository;

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getAllCity(String code,String name, Integer page, Integer pageSize) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        Page<City> pagedResult = cityRepository.findAll((root, query, cb) -> {
            query.orderBy(cb.asc(root.get("code")));
            return cb.and(
                    code == null ? cb.isTrue(cb.literal(true)) : cb.equal(root.get("code"), code),
                    name == null ? cb.isTrue(cb.literal(true)) : cb.like(root.get("name"),"%"+name+"%")
            );
        },paging);
        return new ResponsePagination(
                pagedResult.getContent(),
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getAllDistrict(String code,String name, String cityCode, Integer page, Integer pageSize) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        Page<District> pagedResult = districtRepository.findAll((root, query, cb) -> {
            query.orderBy(cb.asc(root.get("code")));
            Join<District, City> cityJoin = root.join("city",JoinType.LEFT);
            return cb.and(
                    code == null ? cb.isTrue(cb.literal(true)) : cb.equal(root.get("code"), code),
                    name == null ? cb.isTrue(cb.literal(true)) : cb.like(root.get("name"),"%"+name+"%"),
                    cityCode == null ? cb.isTrue(cb.literal(true)) : cb.equal(cityJoin.get("code"), cityCode)
            );
        },paging);
        List<District> output = pagedResult.getContent();
        return new ResponsePagination(
                output,
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getAllCommune(String code,String name, String cityCode, String districtCode, Integer page, Integer pageSize) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        Page<Commune> pagedResult = communeRepository.findAll((root, query, cb) -> {
            query.orderBy(cb.asc(root.get("code")));
            Join<Commune, District> districtJoin = root.join("district", JoinType.LEFT);
            Join<District, City> cityJoin = districtJoin.join("city",JoinType.LEFT);
            return cb.and(
                    code == null ? cb.isTrue(cb.literal(true)) : cb.equal(root.get("code"), code),
                    name == null ? cb.isTrue(cb.literal(true)) : cb.like(root.get("name"),"%"+name+"%"),
                    districtCode == null ? cb.isTrue(cb.literal(true)) : cb.equal(districtJoin.get("code"), districtCode),
                    cityCode == null ? cb.isTrue(cb.literal(true)) : cb.equal(cityJoin.get("code"), cityCode)
            );
        },paging);
        return new ResponsePagination(
                pagedResult.getContent(),
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

    @Override
    public City getCityById(String id) {
        return cityRepository.findByCode(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public District getDistrictById(String id) {
        return districtRepository.findByCode(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public Commune getCommuneById(String id) {
        return communeRepository.findByCode(id).orElseThrow(RuntimeException::new);
    }
}
