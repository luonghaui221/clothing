package com.clotl.service.impl;

import com.clotl.DTO.*;
import com.clotl.entity.*;
import com.clotl.exception.DoesNotExistException;
import com.clotl.exception.ParameterInvalid;
import com.clotl.mapper.OrderMapper;
import com.clotl.mapper.ProductMapper;
import com.clotl.repository.*;
import com.clotl.security.CustomUserDetails;
import com.clotl.service.AddressService;
import com.clotl.service.OrderService;
import com.clotl.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CartRepository cartRepository;
    @Autowired
    VariantsRepository variantsRepository;
    @Autowired
    VariantSizeRepository variantSizeRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    AddressService addressService;

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getListOrder(String email, String keywords, List<String> statuses, String phoneNumber, Date fromDate, Date toDate, Integer page, Integer pageSize, String orderBy, String orderDirection) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        String finalOrderBy = orderDirection.isEmpty() ? "desc" : orderDirection;
        Page<Order> pagedResult = orderRepository.findAll((root, query, cb) -> {
            try {
                if (finalOrderBy.equalsIgnoreCase("ASC")) query.orderBy(cb.asc(root.get(orderBy)));
                else query.orderBy(cb.desc(root.get(orderBy)));
            } catch (IllegalStateException e) {
                query.orderBy(cb.desc(root.get("createAt")));
            }
            return cb.and(
                    email == null ? cb.isTrue(cb.literal(true)) : cb.equal(cb.lower(root.get("email")), email.toLowerCase()),
                    keywords == null ? cb.isTrue(cb.literal(true)) : cb.or(cb.like(root.get("id"),"%"+keywords+"%"),cb.like(root.get("email"),"%"+keywords+"%")),
                    phoneNumber == null ? cb.isTrue(cb.literal(true)) : cb.equal(root.get("phoneNumber"), phoneNumber),
                    (statuses != null && statuses.size() > 0)  ? root.get("statusValue").in(statuses) : cb.isTrue(cb.literal(true)),
                    cb.between(cb.function("date", Date.class, root.get("createAt")), fromDate, toDate));
        }, paging);
        List<Order> output = pagedResult.getContent();
        List<OrderDTO> orderDTOS = orderMapper.toListOrderDTO(output);
        orderDTOS.forEach(x -> {
            try{
                String[] address = x.getAddress().split("-");
                String city = addressService.getCityById(address[0]).getName();
                String district = addressService.getDistrictById(address[1]).getName();
                String commune = addressService.getCommuneById(address[2]).getName();
                x.setAddressName(String.join(", ", Arrays.asList(city, district, commune)));
            }catch (RuntimeException e){
                x.setAddressName(x.getAddress());
            }
        });

        return new ResponsePagination(
                orderDTOS,
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public OrderDTO getOderById(String id) {
        return orderMapper.toDTO(orderRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("Order id does not exist!")));
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public OrderDTO create(OrderDTO orderDTO) {
        CustomUserDetails user = Utils.getCurrentUser();
        Account account = null;
        if(user == null){
            account = accountRepository.findById("CUSTOMER").orElseThrow(RuntimeException::new);
        }else{
            account = accountRepository
                    .findAccountByEmail(user.getUsername())
                    .orElseThrow(()->new DoesNotExistException("Unauthenticated"));
        }
        Order order = orderMapper.toOrder(orderDTO);
        Set<OrderItem> items = new HashSet<>();
        if(orderDTO.getCarts() != null && orderDTO.getCarts().size() > 0){

            order.setId(Utils.generateString(12));
            order.setStatusValue(OrderStatus.PENDING.getValue());
            order.setAccount(account);
            List<CartItem> carts = new ArrayList<>();
            if(user != null) carts = cartRepository.findAllByEmail(Utils.getCurrentUser().getUsername());
            Map<String, Variants> variantMap = variantsRepository.findAllById(
                    orderDTO.getCarts().stream().map(CartItemDTO::getVariantsId).collect(Collectors.toSet()))
                    .stream().collect(Collectors.toMap(Variants::getId, Function.identity()));
            double totalPrice = 0;
            double salePrice = 0;
            for(CartItemDTO ci : orderDTO.getCarts()){
                Optional<VariantsSize> variantSize = variantSizeRepository.findById(ci.getVariantsId(),ci.getSize());
                variantSize.ifPresentOrElse(x -> {
                    x.setQuantity(x.getQuantity() - ci.getQuantity());
                },() -> {throw new RuntimeException("Product not enough");});
                OrderItem o = new OrderItem();
                o.setQuantity(ci.getQuantity());
                o.setSize(ci.getSize());
                Variants variant = variantMap.get(ci.getVariantsId());
                if(variant == null) throw new DoesNotExistException("Variants does not exist!");
                o.setVariants(variant);
                o.setOrder(order);
                o.setId(new OrderItemId(order.getId(),variant.getId()));
                totalPrice += o.getVariants().getProduct().getPrice() * o.getQuantity();
                salePrice += (o.getVariants().getProduct().getSalePrice() == null ? 0 : o.getVariants().getProduct().getSalePrice()) * o.getQuantity();
                items.add(o);
            }
            order.setOrderItems(items);
            order.setTotalPrice(totalPrice);
            order.setSalePrice(salePrice);
            carts.removeIf(c -> items.stream()
                    .noneMatch(i -> c.getVariants().getId().equals(i.getVariants().getId()) && c.getSize().equals(i.getSize())));
            orderRepository.saveAndFlush(order);
            cartRepository.deleteAllInBatch(carts);
        }else{
            throw new ParameterInvalid("No items selected.");
        }
        return orderMapper.toDTO(order);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public OrderDTO update(OrderInfo orderDTO) {
        Order order = orderRepository.findById(orderDTO.getId())
                .orElseThrow(()->new DoesNotExistException("Order does not exist!"));

        if(order.getStatus() != OrderStatus.PENDING)
            throw new RuntimeException("The information of an order can only be modified when its status is PENDING.");

        // only update customer information
        order.setAddress(orderDTO.getAddress());
        order.setNote(orderDTO.getNote());
        order.setPhoneNumber(orderDTO.getPhoneNumber());
        order.setEmail(orderDTO.getEmail());
        order.setFirstName(orderDTO.getFirstName());
        order.setLastName(orderDTO.getLastName());

        orderRepository.saveAndFlush(order);
        return orderMapper.toDTO(order);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public void confirm(String id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("Order does not exist!"));
        if(order.getStatus() == OrderStatus.CANCELED)
            throw new RuntimeException("Order was canceled!");
        if(order.getStatus() != OrderStatus.PENDING)
            throw new RuntimeException("An order can only be confirmed when its status is PENDING.");
        order.setStatusValue(OrderStatus.READY_TO_PICK.getValue());

        orderRepository.saveAndFlush(order);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public void cancel(String id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("Order does not exist!"));
        if(order.getStatus() == OrderStatus.CANCELED)
            throw new RuntimeException("Order was canceled!");
        if(!List.of(OrderStatus.PENDING,OrderStatus.READY_TO_PICK).contains(order.getStatus()))
            throw new RuntimeException("An order can only be canceled before picked.");
        order.setStatusValue(OrderStatus.CANCELED.getValue());

        orderRepository.save(order);
    }

}
