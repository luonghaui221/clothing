package com.clotl.service.impl;

import com.clotl.entity.Product;
import com.clotl.entity.Variants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class ProductActionImpl implements Callable<Product> {
    private JsonNode item;
    private ProductActionImpl(){}
    public ProductActionImpl(JsonNode node){
        this.item = node;
    }
    private Element getScriptTag(Elements elements){
        for(Element e : elements){
            if(e.data().startsWith("window.DATA_STORE")){
                return e;
            }
        }
        return null;
    }

    private JsonNode getProductInfoNode(String productID) {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            Document document = Jsoup.connect("https://www.adidas.com.vn"+item.path("link").asText()).get();
            Elements elements = document.getElementsByTag("script");
            Element scriptTag = getScriptTag(elements);
            if(scriptTag != null){
                String script = scriptTag.data()
                        .replaceAll("(window.DATA_STORE = JSON.parse\\(\\\")|(\\\"\\);)","")
                        .replaceAll("\\\\\"", "\"")
                        .replaceAll("\\\\\\\"","\"");
                JsonNode jsonObject = objectMapper.readTree(script);
                JsonNode item = jsonObject.path("productStore").path("products").path(productID.trim()).path("data");
                return item;
            }else{
                return null;
            }
        }catch (IOException e){
            return null;
        }
    }

    private Product crawlProduct() throws IOException {
        JsonNode infoNode = getProductInfoNode(item.path("productId").asText());
        if(infoNode!=null){
            JsonNode variantsNode = infoNode.path("product_link_list");
            Product product = new Product(
                    item.path("displayName").asText(),
                    item.path("price").asInt(),
                    infoNode.path("product_description").toPrettyString(),
                    new Date(),
                    null
            );
            List<Variants> variantsSet = new ArrayList<>();
            variantsSet.add(new Variants(item.path("productId").asText(),infoNode.path("attribute_list").path("color").asText()));
            product.setVariants(variantsSet);
            return product;
        }else {
            return null;
        }
    }


    @Override
    public Product call() throws Exception {
        Product result = crawlProduct();
        System.out.println(item.path("productId"));
        return result;
    }
}
