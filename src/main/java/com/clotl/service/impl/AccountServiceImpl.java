package com.clotl.service.impl;

import com.clotl.DTO.AccountDTO;
import com.clotl.DTO.OrderDTO;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Account;
import com.clotl.entity.Order;
import com.clotl.entity.Role;
import com.clotl.exception.AlreadyExistException;
import com.clotl.exception.DoesNotExistException;
import com.clotl.mapper.AccountMapper;
import com.clotl.repository.AccountRepository;
import com.clotl.repository.RoleRepository;
import com.clotl.security.CustomUserDetails;
import com.clotl.service.AccountService;
import com.clotl.utils.SnowFlakeIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService, UserDetailsService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    SnowFlakeIdGenerator idGenerator;

    @Override
    public Account findById(String id) {
        return accountRepository.findById(id).orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public AccountDTO getById(String id) {
        Account account = accountRepository.findById(id).orElseThrow(()->new DoesNotExistException("Account does not exist!"));
        AccountDTO dto = accountMapper.toAccountDTO(account);
        dto.setRoleIds(account.getRoles().stream().map(Role::getId).collect(Collectors.toList()));
        return dto;
    }

    @Override
    public Account findByUserEmailAndPassword(String email,String password) {
        return accountRepository.findUserByEmailAndPassword(email,password).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository
                .findAccountByEmail(username)
                .orElseThrow(()->new UsernameNotFoundException("Username not found"));
        account.setReviews(null);
        account.setCartItems(null);
        account.setOrders(null);
        List<String> roles = account.getRoles().stream().map(Role::getCode).collect(Collectors.toList());
        return new CustomUserDetails(account,roles);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public AccountDTO resgisterAccount(AccountDTO accountDTO) {
        if(accountRepository.existsAccountByEmail(accountDTO.getEmail())){
            throw new AlreadyExistException("email already exists");
        }
        Account account = accountMapper.toAccount(accountDTO);
        account.setId(Long.toString(idGenerator.getNextId()));
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        Optional<Role> role = roleRepository.findRoleByCode("customer");
        role.ifPresent(r -> {
            r.getAccounts().add(account);
            account.setRoles(Arrays.asList(r));
        });
        return accountMapper.toAccountDTO(accountRepository.save(account));
    }

    @Override
    public AccountDTO createAccount(AccountDTO accountDTO) {
        if(accountRepository.existsAccountByEmail(accountDTO.getEmail())){
            throw new AlreadyExistException("email already exists");
        }
        Account account = accountMapper.toAccount(accountDTO);
        account.setId(Long.toString(idGenerator.getNextId()));
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        if(accountDTO.getRoleIds() != null){
            accountDTO.getRoleIds().forEach(x -> {
                Role role = roleRepository.getReferenceById(x);
                if(role.getAccounts() == null) role.setAccounts(new ArrayList<>());
                account.setRoles(new ArrayList<>());
                account.getRoles().add(role);
                role.getAccounts().add(account);
            });
        }
        return accountMapper.toAccountDTO(accountRepository.save(account));
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public AccountDTO updateAccount(AccountDTO accountDTO) {
        Optional<Account> account = accountRepository.findById(accountDTO.getId());
        if(account.isEmpty()) throw new DoesNotExistException("Account does not exist!");
        account.ifPresent(x -> {
            x.setAddress(accountDTO.getAddress());
            x.setPhone(accountDTO.getPhone());
            x.setLastName(accountDTO.getLastName());
            x.setFirstName(accountDTO.getFirstName());
            if(accountDTO.getRoleIds() != null){
                List<Role> roles = roleRepository.findAllById(accountDTO.getRoleIds());
                x.getRoles().clear();
                accountRepository.save(x);
                roles.forEach(role -> {
                    x.getRoles().add(role);
                    if(role.getAccounts() == null) role.setAccounts(new ArrayList<>());
                    role.getAccounts().add(x);
                });
            }
            try {
                x.setBirthday(new SimpleDateFormat("yyyy/MM/dd").parse(accountDTO.getBirthday()));
            } catch (ParseException e) {
                x.setBirthday(null);
            }
            x.setGender(accountDTO.isGender());
            x.setLocked(accountDTO.isLocked());
            accountRepository.save(x);
        });
        return accountDTO;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public void deleteAccount(String id) {
        Optional<Account> account = accountRepository.findById(id);
        account.ifPresentOrElse(x -> {
            x.setLocked(true);
            accountRepository.save(x);
        },() -> {throw new DoesNotExistException("Account does not exist!");});
    }

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getListAccount(String email, String keywords, List<String> statuses, String phoneNumber, Integer page, Integer pageSize, String orderBy, String orderDirection) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        String finalOrderBy = orderDirection.isEmpty() ? "desc" : orderDirection;
        Page<Account> pagedResult = accountRepository.findAll((root, query, cb) -> {
            try {
                if (finalOrderBy.equalsIgnoreCase("ASC")) query.orderBy(cb.asc(root.get(orderBy)));
                else query.orderBy(cb.desc(root.get(orderBy)));
            } catch (IllegalStateException e) {
                query.orderBy(cb.desc(root.get("id")));
            }
            return cb.and(
                    email == null ? cb.isTrue(cb.literal(true)) : cb.equal(cb.lower(root.get("email")), email.toLowerCase()),
                    keywords == null ? cb.isTrue(cb.literal(true)) : cb.or(cb.like(cb.concat(root.get("firstName"),root.get("lastName")),"%"+keywords+"%"),cb.like(root.get("email"),"%"+keywords+"%")),
                    phoneNumber == null ? cb.isTrue(cb.literal(true)) : cb.equal(root.get("phone"), phoneNumber),
                    (statuses != null && statuses.size() > 0)  ? root.get("locked").in(statuses) : cb.isTrue(cb.literal(true)));
        }, paging);
        List<Account> output = pagedResult.getContent();
        List<AccountDTO> accountDTOs = accountMapper.toListAccountDTO(output);
        return new ResponsePagination(
                accountDTOs,
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

}
