package com.clotl.service.impl;

import com.clotl.DTO.CartItemDTO;
import com.clotl.DTO.CartVariantsInfo;
import com.clotl.entity.CartItem;
import com.clotl.entity.CartItemId;
import com.clotl.entity.Variants;
import com.clotl.exception.DoesNotExistException;
import com.clotl.mapper.CartItemMapper;
import com.clotl.repository.AccountRepository;
import com.clotl.repository.CartRepository;
import com.clotl.repository.VariantsRepository;
import com.clotl.security.CustomUserDetails;
import com.clotl.service.CartService;
import com.clotl.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    CartRepository cartRepository;
    @Autowired
    VariantsRepository variantsRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CartItemMapper cartItemMapper;

    @Override
    @Transactional(readOnly = true)
    public List<CartVariantsInfo> getListCart(String userId) {
        List<CartItem> cartItems = cartRepository.findAllByUserId(userId);
        List<CartVariantsInfo> cartVariantsInfos = new ArrayList<>();
        cartItems.forEach(item -> {
            cartVariantsInfos.add(new CartVariantsInfo(
                    item.getVariants().getId(),
                    item.getVariants().getProduct().getName(),
                    item.getVariants().getName(),
                    item.getQuantity(),
                    item.getSize(),
                    item.getVariants().getThumbnail(),
                    item.getVariants().getProduct().getPrice(),
                    item.getVariants().getProduct().getSalePrice()
            ));
        });
        return cartVariantsInfos;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public CartItemDTO addToCart(CartItemDTO cartItemDTO) {
        CustomUserDetails userDetails = Utils.getCurrentUser();
        Optional<CartItem> cartItem = cartRepository.findByUserIdAndVariantsId(userDetails.getAccount().getId(), cartItemDTO.getVariantsId());
        cartItem.ifPresentOrElse(c -> {
            c.setQuantity(c.getQuantity() + cartItemDTO.getQuantity());
            cartRepository.save(c);
        }, () -> {
            CartItem item = new CartItem();
            item.setAccount(userDetails.getAccount());
            Variants variants = variantsRepository.findById(cartItemDTO.getVariantsId())
                    .orElseThrow(() -> new DoesNotExistException("variantsID does not exists"));
            item.setVariants(variants);
            item.setSize(cartItemDTO.getSize());
            item.setId(new CartItemId(variants.getId(), userDetails.getAccount().getId()));
            item.setQuantity(cartItemDTO.getQuantity());
            cartRepository.save(item);
        });
        return cartItemMapper.toCartItemDTO(cartItem.orElse(null));
    }

    @Override
    public List<CartItemDTO> addAllToCart(List<CartItemDTO> cartItemDTO) {
        List<CartItemDTO> cartItemDTOS = new ArrayList<>();
        for (CartItemDTO item : cartItemDTO) {
            cartItemDTOS.add(addToCart(item));
        }
        return cartItemDTOS;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void removeFromCart(CartItemDTO cartItemDTO) {
        if (cartItemDTO.getVariantsId().isBlank() || cartItemDTO.getSize().isBlank()) {
            throw new IllegalArgumentException("VariantsId or size invalid");
        }
        CustomUserDetails userDetails = Utils.getCurrentUser();
        Optional<CartItem> cartItem = cartRepository.findByUserIdAndVariantsId(userDetails.getAccount().getId(), cartItemDTO.getVariantsId());
        cartItem.ifPresent(c -> {
            cartRepository.delete(c);
        });
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public CartItemDTO updateCart(CartItemDTO cartItemDTO) {

        CustomUserDetails userDetails = Utils.getCurrentUser();
        CartItem cartItem = cartRepository
                .findByUserIdAndVariantsId(userDetails.getAccount().getId(), cartItemDTO.getVariantsId())
                .orElseThrow(() -> new DoesNotExistException("CartItem does not exists"));
        cartItem.setQuantity(cartItemDTO.getQuantity());
        return cartItemMapper.toCartItemDTO(cartItem);
    }
}
