package com.clotl.service.impl;

import com.clotl.entity.Image;
import com.clotl.entity.Product;
import com.clotl.entity.Variants;
import com.clotl.repository.ImageRepository;
import com.clotl.repository.ProductRepository;
import com.clotl.repository.VariantsRepository;
import com.clotl.service.CrawlDataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

@Service
public class CrawlDataServiceImpl implements CrawlDataService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    VariantsRepository variantsRepository;
    @Autowired
    ImageRepository imageRepository;

    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    @Transactional(rollbackFor = {Exception.class,Throwable.class})
    public List<Product> crawlDataProduct(){
        try{
            List<Product> products = new ArrayList<>();
            ExecutorService service = Executors.newFixedThreadPool(8);
            List<Callable<Product>> callables = new ArrayList<>();
            for(int i=1;i<=89;i++){
                JsonNode item = getItemsInPage(i);
                for(JsonNode n : item){
                    callables.add(new ProductActionImpl(n));
                }
            }
            List<Future<Product>> futures = service.invokeAll(callables);
            for(Future<Product> p : futures){
                if(p.get()!=null){
                    String name = p.get().getName().replaceAll(" ","-");
                    int size = products.size();
                    int count = 0;
                    for(int i=0;i<size;i++){
                        if(products.get(i).getName().replaceAll(" ","-").equals(name)){
//                            products.get(i).addVariation(p.get().getVariants().get(0));
                            break;
                        }
                        count++;
                    }
                    if(count == size || size == 0){
                        products.add(p.get());
                    }
                }
            }
            return productRepository.saveAll(products);
        }catch (JsonProcessingException | InterruptedException | ExecutionException e){
            System.out.println("Có lỗi xảy ra!");
            return Collections.emptyList();
        }
    }

    @Override
    public void crawlImage() {
        try{
//            List<Variants> variants = variantsRepository.findAll();
            List<Variants> variants = variantsRepository.findLimit(1,0);
            ExecutorService service = Executors.newFixedThreadPool(10);
            for(Variants v : variants){
                Future<List<Image>> future = service.submit(new ImageActionImpl(v.getId()));
                List<Image> images = future.get();
                v.setImages(images);
                variantsRepository.save(v);
                System.out.println("success "+v.getId());
            }
        }catch (InterruptedException | ExecutionException e){

        }
    }

    private JsonNode getItemsInPage(int page) throws JsonProcessingException {
        WebClient wc = WebClient.builder()
                .baseUrl("https://www.adidas.com.vn/api/plp/content-engine")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT,MediaType.APPLICATION_JSON_VALUE)
                .build();

        var resp = wc.get().uri("/search?sitePath=vi&query=all&start="+(48*(page-1))).retrieve()
                .bodyToMono(String.class)
                .block();
        JsonNode root = objectMapper.readValue(resp,JsonNode.class);
        return root.path("raw").path("itemList").path("items");
    }

}
