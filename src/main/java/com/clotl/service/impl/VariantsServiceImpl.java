package com.clotl.service.impl;

import com.clotl.entity.Variants;
import com.clotl.repository.VariantsRepository;
import com.clotl.service.VariantsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VariantsServiceImpl implements VariantsService {
    @Autowired
    VariantsRepository variantsRepository;
    @Override
    public List<Variants> saveAll(List<Variants> variants) {
        return variantsRepository.saveAll(variants);
    }
}
