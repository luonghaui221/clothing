package com.clotl.service.impl;

import com.clotl.DTO.CategoryDTO;
import com.clotl.DTO.CategoryTreeData;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Category;
import com.clotl.exception.DoesNotExistException;
import com.clotl.mapper.CategoryMapper;
import com.clotl.repository.CategoryRepository;
import com.clotl.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    @Transactional(readOnly = true)
    public List<CategoryTreeData> getAllCategory() {
        return categoryMapper.toListCategoryTreeData(categoryRepository.findAllAsTreeData());
    }

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getListCategory(Integer page, Integer pageSize) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        Page<Category> categories = categoryRepository.findAll(((root, query, cb) -> {
            query.orderBy(cb.desc(root.get("id")));
            return cb.and();
        }),paging);
        List<Category> result = categories.getContent();
        result.forEach(x -> x.setChildren(null));
        return new ResponsePagination(
                categoryMapper.toListCategoryDTO(result),
                page,
                categories.getTotalPages(),
                (int) categories.getTotalElements(),
                categories.getSize()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public CategoryDTO getCategoryById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        category.ifPresentOrElse(ct -> {
        },()->{throw new DoesNotExistException("category does not exist");});
        return categoryMapper.ToCategoryDto(category.orElse(null));
    }

    @Override
    @Transactional
    public CategoryDTO addCategories(CategoryDTO category) {
        Category c = categoryMapper.ToCategory(category);
        categoryRepository.save(c);
        return categoryMapper.ToCategoryDto(c);
    }

    @Override
    @Transactional
    public CategoryDTO updateCategories(CategoryDTO category) {
        Optional<Category> exists = categoryRepository.findById(category.getCategoryId());
        exists.ifPresentOrElse(c->{
            c.setName(category.getCategoryName());
            c.setUrl(category.getUrl());
        },()-> {throw new DoesNotExistException("id does not exists");});
        return categoryMapper.ToCategoryDto(exists.orElse(null));
    }

    @Override
    @Transactional
    public void deleteCategories(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        category.ifPresentOrElse(c->{
            categoryRepository.delete(c);
        },()-> {throw new DoesNotExistException("id does not exists");});
    }
}
