package com.clotl.service.impl;

import com.clotl.constant.CONSTANT;
import com.clotl.entity.Image;
import com.clotl.repository.ImageRepository;
import com.clotl.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    ImageRepository imageRepository;
    @Override
    public List<Image> findAllByVariantID(String id) {
        return imageRepository.findAllByVariantId(id);
    }

    @Override
    public byte[] findByName(String name) throws IOException {
        String path = CONSTANT.IMG_DIR+"\\"+name;
        FileInputStream inputStream = new FileInputStream(path);
        try{
            byte[] bytes = StreamUtils.copyToByteArray(inputStream);
            return bytes;
        }finally {
            inputStream.close();
        }
    }

    @Override
    public List<String> saveImages(MultipartFile[] files) throws IOException {
        List<String> paths = new ArrayList<>();
        if(files.length == 1 && files[0] == null) return Collections.emptyList();
        for(MultipartFile file : files){
            if(!isSupported(file)) throw new MultipartException("file type is not supported");
            String fileName = UUID.randomUUID().toString().replaceAll("-","").concat(".jpg");
            file.transferTo(new File(CONSTANT.IMG_DIR+"\\"+fileName));
            paths.add(fileName);
        }
        return paths;
    }

    private boolean isSupported(MultipartFile file){
        return CONSTANT.MIME_IMG_SUPPORTED.contains(file.getContentType());
    }
}
