package com.clotl.service.impl;

import com.clotl.DTO.ResponseObject;
import com.clotl.entity.Product;
import com.clotl.entity.Variants;
import com.clotl.mapper.ProductMapper;
import com.clotl.repository.ProductRepository;
import com.clotl.repository.VariantsRepository;
import com.clotl.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import weka.classifiers.lazy.IBk;
import weka.core.*;
import weka.core.neighboursearch.LinearNNSearch;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToNominal;
import weka.filters.unsupervised.instance.RemoveWithValues;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecommendServiceImpl implements RecommendService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    VariantsRepository variantsRepository;
    @Autowired
    ProductMapper productMapper;
    private Instances data;
    private final Attribute id = new Attribute("id");
    private final Attribute color = new Attribute("color",(List<String>) null);
    private final Attribute size = new Attribute("size",(List<String>) null);
    private final Attribute price = new Attribute("price");
    private final Attribute category = new Attribute("category",(List<String>) null);
    private final Attribute classification = new Attribute("classification");


    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void getRecommendations() throws Exception {
        // Convert string attributes to nominal attributes
        getDataSet();
        StringToNominal filter = new StringToNominal();
        filter.setAttributeRange("2-" + data.numAttributes());
        filter.setInputFormat(data);
        data = Filter.useFilter(data, filter);

        RemoveWithValues filter2 = new RemoveWithValues();
        filter2.setInputFormat(data);
        data = Filter.useFilter(data, filter2);

        System.out.println("successs collect dataset");
    }

    @Transactional(readOnly = true)
    public void getDataSet(){
        // Load the dataset from the database
        List<Variants> variantsList = variantsRepository.findAll();
        ArrayList<Attribute> attributes = new ArrayList<>(Arrays.asList(id,color,size,price,category,classification));

        data = new Instances("ProductRecommendation", attributes, 0);
        // Add instances to dataset
        for (Variants variants : variantsList) {
            Instance instance = new DenseInstance(6);
            setValueForInstance(variants, instance);
            data.add(instance);
        }
        data.setClass(id);
    }

    private double classification(Variants v){
        boolean isOutOfStock = v.getSizes().stream().noneMatch(x -> x.getQuantity() > 0);
        return (!v.isDeleted() && !isOutOfStock && v.getProduct().getPrice() > 0) ? 1.0 : 0;
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseObject getRecommendations(String id,int kNearest) {
        try{
            Variants variant = variantsRepository.findById(id).orElseThrow(RuntimeException::new);
            Instance targetProduct = new DenseInstance(6);
            setValueForInstance(variant, targetProduct);
            LinearNNSearch knn = new LinearNNSearch();
            knn.setDistanceFunction(new ManhattanDistance());
            knn.setInstances(data);
            Instances result = knn.kNearestNeighbours(targetProduct,kNearest);
            Set<Long> ids = new HashSet<>();
            for (int i = 0;i < result.numInstances(); i++){
                if(variant.getProduct().getId() == (long)result.instance(i).value(0)) continue;
                if(ids.size() >= kNearest) break;
                ids.add((long) result.instance(i).value(0));
            }
            List<Product> products = productRepository.findAllById(ids);
            return new ResponseObject(HttpStatus.OK.value(),"",productMapper.toListProductSlimDTO(products));
        }catch (Exception ex){
            return new ResponseObject(HttpStatus.OK.value(),"",productMapper.toListProductSlimDTO(new ArrayList<>()));
        }
    }

    private void setValueForInstance(Variants variant, Instance targetProduct) {
        targetProduct.setValue(id,variant.getProduct().getId());
        targetProduct.setValue(color,variant.getName());
        targetProduct.setValue(price,variant.getProduct().getPrice());
        String sizes = variant.getSizes().stream().map(x -> x.getId().getSizeCode()).collect(Collectors.joining(","));
        String categories = variant.getSizes().stream().map(x -> x.getId().getSizeCode()).collect(Collectors.joining(","));
        targetProduct.setValue(size,sizes);
        targetProduct.setValue(category,categories);
        targetProduct.setValue(classification,classification(variant));
    }
}
