package com.clotl.service.impl;

import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.*;
import com.clotl.mapper.ProductMapper;
import com.clotl.mapper.VartiantsMapper;
import com.clotl.repository.*;
import com.clotl.service.IDashboardService;
import com.clotl.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
public class DasboardServiceImpl implements IDashboardService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    ProductRepository _productRepository;
    @Autowired
    ProductMapper productMapper;
    @Override
    public double getRevenue(Integer day, Integer month, Integer year) {
        List<Order> orders = orderRepository.findAll((root, query, cb) -> {
            Expression<Integer> dayExpression = cb.function("DAY",Integer.class,root.get("createAt"));
            Expression<Integer> monthExpression = cb.function("MONTH",Integer.class,root.get("createAt"));
            Expression<Integer> yearExpression = cb.function("YEAR",Integer.class,root.get("createAt"));
            return cb.and(
                    day == null ? cb.isTrue(cb.literal(true)) : cb.equal(dayExpression, day),
                    month == null ? cb.isTrue(cb.literal(true)) : cb.equal(monthExpression, month),
                    year == null ? cb.isTrue(cb.literal(true)) : cb.equal(yearExpression, year),
                    cb.equal(root.get("statusValue"), OrderStatus.DELIVERED.getValue())
            );
        });

        return orders.stream().map(elm -> elm.getSalePrice() > 0 ? elm.getSalePrice() : elm.getTotalPrice()).reduce(Double::sum).orElse(0.0);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getProductOutOfStock(String name, Double minPrice, Double maxPrice, List<Long> categories, List<String> sizes,
                                     Integer page, Integer pageSize, String orderBy, String orderDirection) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        String finalOrderBy = orderDirection.isEmpty() ? "desc" : orderDirection;
        Page<Product> pagedResult = _productRepository.findAll((root, query, cb) -> {
            try{
                if(finalOrderBy.equals("asc")) query.orderBy(cb.asc(root.get(orderBy)));
                else query.orderBy(cb.desc(root.get(orderBy)));
            }catch (IllegalStateException e){
                query.orderBy(cb.desc(root.get("id")));
            }
            Join<Product, Category> categoryJoin = root.join("categories", JoinType.LEFT);
            Join<Product, Variants> variantsJoin = root.join("variants");
            Join<Variants,VariantsSize> variantsSizeJoin = variantsJoin.join("sizes",JoinType.LEFT);
            Join<VariantsSize,Size> sizeJoin = variantsSizeJoin.join("size");
            query.distinct(true);
            return cb.and(
                    cb.isFalse(root.get("isDeleted")),
                    (categories != null && categories.size() > 0) ? categoryJoin.get("id").in(categories) : cb.isTrue(cb.literal(true)),
                    (sizes != null && sizes.size() > 0) ? sizeJoin.get("code").in(sizes) : cb.isTrue(cb.literal(true)),
                    name == null ? cb.isTrue(cb.literal(true)) : cb.like(root.get("name"), "%" + name + "%"),
                    cb.between(root.get("price"), minPrice, maxPrice),
                    cb.or(cb.isNull(variantsSizeJoin.get("quantity")),cb.equal(variantsSizeJoin.get("quantity"),0))
            );
        }, paging);
        List<Product> output = pagedResult.getContent();
        output.forEach(o -> o.getVariants().size());
        return new ResponsePagination(
                productMapper.toListProductSlimDTO(output),
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }
}
