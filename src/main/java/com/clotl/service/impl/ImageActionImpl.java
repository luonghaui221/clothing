package com.clotl.service.impl;

import com.clotl.constant.CONSTANT;
import com.clotl.entity.Image;
import com.clotl.entity.ImageType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

public class ImageActionImpl implements Callable<List<Image>> {
    private String id;
    public ImageActionImpl() {
    }

    public ImageActionImpl(String id) {
        this.id = id;
    }

    private String dowloadImage(String src, String name) {
        InputStream in = null;
        OutputStream out = null;
        try{
            URL url = new URL(src.replaceAll("THUMBNAIL",""));
            in = url.openStream();
            String path =  CONSTANT.IMG_DIR + "\\" + name;
            out = new BufferedOutputStream(new FileOutputStream(path));
            for (int b; (b = in.read()) != -1;) {
                out.write(b);
            }
            out.close();
            in.close();
            return name;
        }
        catch (IOException e) {
            System.out.println("Lỗi khi dowload link:"+src);
            //e.printStackTrace();
            return "";
        }
    }
    private List<String> getListImageUrl(String id){
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            List<String> urls = new ArrayList<>();
            String resp = WebClient
                    .create("https://www.adidas.com.vn/api/search/product")
                    .get()
                    .uri("/{id}",id)
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
            JsonNode root = objectMapper.readTree(resp);
            JsonNode imgs = root.path("images");
            urls.add(root.path("image").path("src").asText()+"THUMBNAIL");
            for(JsonNode node : imgs){
                urls.add(node.path("src").asText());
            }
            return urls;
        }catch (JsonProcessingException e){
            return Collections.emptyList();
        }
    }
    @Override
    public List<Image> call() throws Exception {
        List<String> urls = getListImageUrl(this.id);
        List<Image> images = new ArrayList<>();
        for(String url : urls){
            String path = dowloadImage(url, UUID.randomUUID().toString().replaceAll("-","").concat(".jpg"));
            if(url.contains("THUMBNAIL")){
                images.add(new Image(path));
            }else{
                images.add(new Image(path));
            }
        }
        return images;
    }
}
