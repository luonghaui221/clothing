package com.clotl.service.impl;

import com.clotl.DTO.RequestPagination;
import com.clotl.entity.Tag;
import com.clotl.repository.TagRepository;
import com.clotl.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public Page<Tag> findAll(RequestPagination pagination) {
        return tagRepository.findAll(PageRequest.of(pagination.getPage()-1,pagination.getSize()));
    }

    @Override
    @Transactional
    public Tag addTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    @Transactional
    public Tag updateTag(Tag tag) {
        Tag current = tagRepository.findById(tag.getId()).orElseThrow();
        current.setName(tag.getName());
        return current;
    }

    @Override
    @Transactional
    public void deleteTag(Long id) {
        Tag current = tagRepository.findById(id).orElseThrow();
        tagRepository.delete(current);
    }
}
