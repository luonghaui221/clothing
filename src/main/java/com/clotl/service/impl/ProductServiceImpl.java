package com.clotl.service.impl;

import com.clotl.DTO.*;
import com.clotl.entity.*;
import com.clotl.exception.DoesNotExistException;
import com.clotl.mapper.ProductMapper;
import com.clotl.mapper.VartiantsMapper;
import com.clotl.repository.*;
import com.clotl.service.ProductService;
import org.hibernate.Session;
import org.hibernate.engine.spi.EntityKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.EntityType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository _productRepository;
    @Autowired
    SizeRepository _sizeRepository;
    @Autowired
    VariantsRepository _variantsRepository;
    @Autowired
    VariantSizeRepository _variantSizeRepository;
    @Autowired
    CategoryRepository _categoryRepository;
    @Autowired
    CollectionRepository _collectionRepository;
    @Autowired
    ImageRepository _imageRepository;
    @Autowired
    TagRepository _tagRepository;
    @Autowired
    CustomRepository _customRepository;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    VartiantsMapper vartiantsMapper;

    @Override
    @Transactional(readOnly = true)
    public ResponsePagination getAll(String name, Double minPrice, Double maxPrice, List<Long> categories, List<String> sizes,
                                           Integer page, Integer pageSize, String orderBy, String orderDirection) {
        Pageable paging = PageRequest.of(page - 1, pageSize);
        String finalOrderBy = orderDirection.isEmpty() ? "desc" : orderDirection;
        Page<Product> pagedResult = _productRepository.findAll((root, query, cb) -> {
            try{
                if(finalOrderBy.equals("asc")) query.orderBy(cb.asc(root.get(orderBy)));
                else query.orderBy(cb.desc(root.get(orderBy)));
            }catch (IllegalStateException e){
                query.orderBy(cb.desc(root.get("id")));
            }
            Join<Product,Category> categoryJoin = root.join("categories", JoinType.LEFT);
            Join<Product,Variants> variantsJoin = root.join("variants");
            Join<Variants,VariantsSize> variantsSizeJoin = variantsJoin.join("sizes",JoinType.LEFT);
            Join<VariantsSize,Size> sizeJoin = variantsSizeJoin.join("size");
            query.distinct(true);
            return cb.and(
                    cb.isFalse(root.get("isDeleted")),
                    (categories != null && categories.size() > 0) ? categoryJoin.get("id").in(categories) : cb.isTrue(cb.literal(true)),
                    (sizes != null && sizes.size() > 0) ? sizeJoin.get("code").in(sizes) : cb.isTrue(cb.literal(true)),
                    name == null ? cb.isTrue(cb.literal(true)) : cb.like(root.get("name"), "%" + name + "%"),
                    cb.between(root.get("price"), minPrice, maxPrice)
            );
        }, paging);
        List<Product> output = pagedResult.getContent();
        output.forEach(o -> o.getVariants().size());
        return new ResponsePagination(
                productMapper.toListProductSlimDTO(output),
                page,
                pagedResult.getTotalPages(),
                (int) pagedResult.getTotalElements(),
                pagedResult.getSize()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductSlimDTO> getAllInIds(List<Long> ids) {
        List<Product> products = _productRepository.findAllById(ids);
        return productMapper.toListProductSlimDTO(products);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ProductDTO save(ProductDTO product) {
        Product newProduct = productMapper.ToProduct(product,_sizeRepository);
        String variantsId = null;
        for(Variants v :newProduct.getVariants()){
            variantsId = generatedId(variantsId);
            v.setId(variantsId);
            for(VariantsSize vs : v.getSizes()){
                vs.setId(new VariantsSizeId(v.getId(),vs.getSize().getCode()));
            }
        }
        productDtoToProduct(newProduct,product);
        Product result = _productRepository.saveAndFlush(newProduct);
        return productMapper.ToProductDTO(result);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ProductDTO update(ProductDTO toUpdate) {
        Product current = _productRepository.findById(toUpdate.getId())
                .orElseThrow(() -> new DoesNotExistException("Product is not exists"));
        productMapper.UpdateExisting(toUpdate,current);
        List<Variants> currentVariants = current.getVariants();
        for(int i=0;i<currentVariants.size();i++){
            Variants v = currentVariants.get(i);
            if(v.isDeleted()) continue;
            VariantsDTO variantsDTO = toUpdate.getVariants().stream()
                    .filter(x -> x.getId() != null && x.getId().equals(v.getId()))
                    .findFirst().orElse(null);
            if(variantsDTO == null){
                v.setDeleted(true);
            }else{
                List<Image> imgs = _imageRepository.findAllByVariantId(variantsDTO.getId());
                List<Image> deletedImgs = imgs.stream().filter(x -> variantsDTO.getImages().stream().noneMatch(y -> y.getId().equals(x.getId()))).collect(Collectors.toList());
                _imageRepository.deleteAllInBatch(deletedImgs);
                variantsDTO.getImages().removeIf(x -> imgs.stream().anyMatch(y -> x.getId().equals(y.getId())));
                v.addImage(variantsDTO.getImages());
                v.setName( variantsDTO.getName() );
                v.setColor( variantsDTO.getColor() );
                v.setThumbnail( variantsDTO.getThumbnail() );
                _variantSizeRepository.deleteAllByVariantsId(v.getId());
                v.getSizes().forEach(x -> {
                    VariantsSize entity = _customRepository.getEntityManager().find(VariantsSize.class,x.getId());
                    if(entity != null) _customRepository.detach(entity);
                });
                v.getSizes().clear();
                List<VariantsSize> newVariantsSizes = new ArrayList<>();
                variantsDTO.getSizes().forEach(sDTO -> {
                    VariantsSize newVariantsSize = new VariantsSize();
                    Size size = _sizeRepository.getReferenceById(sDTO.getSizeCode());
                    newVariantsSize.setId(new VariantsSizeId());
                    newVariantsSize.setQuantity(sDTO.getQuantity());
                    newVariantsSize.setSize(size);
                    newVariantsSize.setVariants(v);
                    newVariantsSizes.add(newVariantsSize);
                });
                _variantSizeRepository.saveAll(newVariantsSizes);
                toUpdate.getVariants().remove(variantsDTO);
            }
        }
        List<Variants> newVariants = vartiantsMapper.convert(toUpdate.getVariants(),_sizeRepository);
        String variantsId = null;
        for(Variants v :newVariants){
            variantsId = generatedId(variantsId);
            v.setId(variantsId);
            for(VariantsSize vs : v.getSizes()){
                vs.setId(new VariantsSizeId(v.getId(),vs.getSize().getCode()));
            }
        }
        current.addListVariant(newVariants);
        productDtoToProduct(current,toUpdate);
        _productRepository.saveAndFlush(current);
        _customRepository.getEntityManager().clear();
        return getById(current.getId());
    }

    @Transactional
    public void productDtoToProduct(Product product,ProductDTO DTO){
        if(product == null) return;
        if(DTO == null) return;
        List<Category> categories = new ArrayList<>();
        List<Tag> tags = new ArrayList<>();
        List<Collection> collections = new ArrayList<>();
        if(DTO.getCategoryIds() != null){
            categories = _categoryRepository.getAllByIdIn(DTO.getCategoryIds());
            for(Long id : DTO.getCategoryIds()){
                if (categories.stream().noneMatch(category -> category.getId().equals(id))){
                    throw new DoesNotExistException(String.format("Category with Id: {%d} not found",id));
                }
            }
        }
        if(DTO.getTagIds() != null){
            tags = _tagRepository.getAllByIdIn(DTO.getTagIds());
            for(Long id : DTO.getCategoryIds()){
                if (tags.stream().noneMatch(tag -> tag.getId().equals(id))){
                    throw new DoesNotExistException(String.format("Tag with Id: {%d} not found",id));
                }
            }
        }
        if(DTO.getCollectionIds() != null){
            collections = _collectionRepository.getAllByIdIn(DTO.getCollectionIds());
            for(Long id : DTO.getCategoryIds()){
                if (collections.stream().noneMatch(collection -> collection.getId().equals(id))){
                    throw new DoesNotExistException(String.format("Collection with Id: {%d} not found",id));
                }
            }
        }
        Product p = product;
        if(product.getId() != null){
            product.getCategories().removeIf(x -> !DTO.getCategoryIds().contains(x.getId()));
            DTO.getCategoryIds().removeIf(x -> product.getCategories().stream().anyMatch(y -> x.equals(y.getId())));
            p = _productRepository.getReferenceById(product.getId());
        }else{
            p.setCategories(new ArrayList<>());
        }
        Product finalP = p;
        DTO.getCategoryIds().forEach(x -> {
            Category c =_categoryRepository.getReferenceById(x);
            finalP.getCategories().add(c);
            c.getProducts().add(finalP);
        });
        _productRepository.save(p);
        product.setTags(tags);
        product.setCollections(collections);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void delete(Long id) {
        Product product = _productRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("id does not exists"));
        product.setDeleted(true);
        _productRepository.save(product);
    }
    @Override
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void forceDelete(Long id) {
        Product product = _productRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("id does not exists"));
        _productRepository.delete(product);
    }

    @Override
    @Transactional(readOnly = true)
    public ProductDTO getById(Long id) {
        Product product = _productRepository.findById(id)
                .orElseThrow(()->new DoesNotExistException("Product does not exists"));
        product.getVariants().removeIf(Variants::isDeleted);
        product.getVariants().forEach(v -> {
            v.setOrderItems(null);
            v.setCartItems(null);
        });
        product.getCategories().forEach(x -> x.setChildren(null));
        product.getReviews().size();
        product.getTags().size();
        product.getCollections().size();
        return productMapper.ToProductDTO(product);
    }

    @Transactional(readOnly = true,propagation = Propagation.REQUIRED)
    public String generatedId(String currentId) {
        if(!StringUtils.hasText(currentId)){
            Optional<Variants> variants = _variantsRepository.findTopByOrderByIdDesc();
            currentId = variants.isPresent() ? variants.get().getId() : "000000";
        }
        StringBuilder lastsID = new StringBuilder(currentId);
        for (int i = lastsID.length() - 1; i >= 0; i--) {
            if (lastsID.charAt(i) == '9') {
                lastsID.setCharAt(i, '0');
                continue;
            }
            if (lastsID.charAt(i) == 'Z') {
                lastsID.setCharAt(i, 'A');
                continue;
            }
            if (
                    (lastsID.charAt(i) >= '0' && lastsID.charAt(i) < '9') ||
                            (lastsID.charAt(i) >= 'A' && lastsID.charAt(i) <= 'Z')
            ) {
                lastsID.setCharAt(i, (char) (lastsID.charAt(i) + 1));
                break;
            }
        }
        return lastsID.toString();
    }
}
