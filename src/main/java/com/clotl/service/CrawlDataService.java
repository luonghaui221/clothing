package com.clotl.service;

import com.clotl.entity.Product;

import java.util.List;

public interface CrawlDataService {
    List<Product> crawlDataProduct();
    void crawlImage();
}
