package com.clotl.service;

import com.clotl.DTO.RequestPagination;
import com.clotl.entity.Tag;
import org.springframework.data.domain.Page;

public interface TagService {
    Page<Tag> findAll(RequestPagination pagination);
    Tag addTag(Tag tag);
    Tag updateTag(Tag tag);
    void deleteTag(Long id);
}
