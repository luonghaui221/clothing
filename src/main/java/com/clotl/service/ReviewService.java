package com.clotl.service;

import com.clotl.DTO.ResponseObject;
import com.clotl.DTO.ResponsePagination;
import com.clotl.DTO.ReviewDTO;

public interface ReviewService {
    ResponsePagination getReviewOfProduct(Long id, Integer page, Integer pageSize);
    ResponseObject createOrUpdateReview(ReviewDTO reviewDTO);
    ResponseObject deleteReview(Long productId);
}
