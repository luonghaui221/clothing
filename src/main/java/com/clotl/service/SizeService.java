package com.clotl.service;

import com.clotl.entity.Size;
import com.clotl.mapper.SizeIdMapper;
import com.clotl.mapper.SizeIdToEntity;
import org.mapstruct.IterableMapping;

import java.util.List;

@SizeIdMapper
public interface SizeService {
    List<Size> findAll();
    Size addSize(Size size);
    Size updateSize(Size size);
    void deleteSize(String sizeCode);
    @SizeIdToEntity
    Size findById(String id);
}
