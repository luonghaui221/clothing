package com.clotl.htttpClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class apiGHN {
    @Autowired
    private WebClient GHN;
    public String getOrderDetail() {
//        RestTemplate restTemplate = new RestTemplate();
//        String ResourceUrl
//                = "https://dev-online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/detail";
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//        httpHeaders.add("token","53200138-02a2-11ed-82ef-e20b2d441066");
//        String requestJson = "{\"order_code\":\"LLFR3M\"}";
//        HttpEntity<String> entity = new HttpEntity<String>(requestJson, httpHeaders);
//        ResponseEntity<String> response
//                = restTemplate.postForEntity(ResourceUrl, entity, String.class);
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        try{
//            JsonNode root = objectMapper.readValue(response.getBody(),JsonNode.class);
//            JsonNode data = root.get("data");
//            JsonNode clientResponseJson = data.deepCopy();
//            ((ObjectNode)clientResponseJson).remove("shop_id");
//            ((ObjectNode)clientResponseJson).remove("client_id");
//            return clientResponseJson.toPrettyString();
//        }catch (IOException e){
//            System.out.println("not found");
//            return "{}";
//        }
        ObjectMapper mp = new ObjectMapper();
        ObjectNode root = mp.createObjectNode();
        root.put("status","200");
        root.put("message","custom json object");
        root.set("data",mp.createObjectNode().put("name","Luong"));
        var resp = GHN.post().uri("/shipping-order/detail")
                .body(BodyInserters.fromValue("{\"order_code\":\"LLFR3M\"}"))
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return root.toPrettyString();
    }
}
