package com.clotl.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
@RestControllerAdvice
public class ApiExceptionHandler{
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleAllException(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), "an error occurred", request.getDescription(false));
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage handleAllException(RuntimeException ex, WebRequest request) {
        return new ErrorMessage(new Date(), ex.getMessage(), request.getDescription(false));
    }

    @ExceptionHandler({DoesNotExistException.class,AlreadyExistException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage existOrNot(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), ex.getMessage(), request.getDescription(false));
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
    public ErrorMessage httpMethodNotSupport(Exception ex, WebRequest request){
        return new ErrorMessage(new Date(), ex.getMessage(), request.getDescription(false));
    }

    @ExceptionHandler({
            IllegalArgumentException.class,
            MethodArgumentTypeMismatchException.class,
            NumberFormatException.class,
            JsonProcessingException.class,
            MismatchedInputException.class,
            HttpMessageNotReadableException.class,
            UnsupportedEncodingException.class,
            MissingPathVariableException.class
    })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage parameterInvalid(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), "Request invalid", request.getDescription(false));
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage modelsInvalid(MethodArgumentNotValidException e,WebRequest request) {
        List<String> message = e.getBindingResult()
                .getAllErrors()
                .stream()
                .map(element -> element.getDefaultMessage())
                .collect(Collectors.toList());
        return new ErrorMessage(new Date(), message, request.getDescription(false));
    }

    @ExceptionHandler({BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage bindingParamerter(BindException e,WebRequest request) {
        List<String> errs = e.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
        return new ErrorMessage(new Date(), errs, request.getDescription(false));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage modelsInvalid(ConstraintViolationException e,WebRequest request) {
        List<String> message = e.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessageTemplate)
                .collect(Collectors.toList());
        return new ErrorMessage(new Date(), message, request.getDescription(false));
    }

    @ExceptionHandler({NoSuchElementException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage doesNotExists(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), ex.getMessage(), request.getDescription(false));
    }

    @ExceptionHandler({MaxUploadSizeExceededException.class, FileSizeLimitExceededException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage filesAreTooLarge(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), "File upload are too large", request.getDescription(false));
    }

    @ExceptionHandler({MultipartException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage fileTypeNotSupported(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), "File type not supported", request.getDescription(false));
    }

    @ExceptionHandler({ParameterInvalid.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage parentCategoryIdDeleted(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), ex.getMessage(), null);
    }

    @ExceptionHandler({BadCredentialsException.class})
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ErrorMessage jwtInvalid(Exception ex, WebRequest request) {
        return new ErrorMessage(new Date(), "Email or password invalid", request.getDescription(false));
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public ErrorMessage notPermission(Exception ex,WebRequest request){
        return new ErrorMessage(new Date(),ex.getMessage(),request.getDescription(false));
    }
}
