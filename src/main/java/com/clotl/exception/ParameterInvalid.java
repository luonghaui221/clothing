package com.clotl.exception;

public class ParameterInvalid extends RuntimeException{
    public ParameterInvalid(String message){
        super(message);
    }
}
