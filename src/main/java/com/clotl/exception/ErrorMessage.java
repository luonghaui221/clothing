package com.clotl.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class ErrorMessage {
    private final Date timestamp;
    private final Object message;
    private final String details;
}
