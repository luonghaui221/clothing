package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "commune")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Commune {
    @Id
    private String id;
    private String name;
    private String code;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "district_code",referencedColumnName = "code")
    @JsonIgnore
    private District district;
}
