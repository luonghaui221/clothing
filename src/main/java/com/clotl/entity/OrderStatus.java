package com.clotl.entity;

import java.util.stream.Stream;

public enum OrderStatus {
    PENDING("pending"),
    READY_TO_PICK("ready_to_pick"),
    PICKING("picking"),
    CANCELED("canceled"),
    PICKED("picked"),
    STORING("storing"),
    DELIVERING("delivering"),
    DELIVERED("delivered"),
    DEFAULT("default");

    public final String value;
    public String getValue(){
        return value;
    }
    OrderStatus(String value){
        this.value = value;
    }
    public static OrderStatus of(String value) {
        return Stream.of(OrderStatus.values())
                .filter(p -> p.getValue().equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
