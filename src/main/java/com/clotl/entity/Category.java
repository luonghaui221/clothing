package com.clotl.entity;


import com.clotl.utils.Utils;
import com.fasterxml.jackson.annotation.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.*;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_category"))
    private Category parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Category> children;

    @ManyToMany(mappedBy = "categories",cascade = { CascadeType.ALL },fetch = FetchType.LAZY)
    private List<Product> products;

    @PrePersist
    public void generatedUrl(){
        String parentUrl = this.parent == null ? "/" : this.parent.getUrl() + "-";
        this.url = parentUrl + Objects.requireNonNull(Utils.removeAccents(this.name)).replaceAll(" ","_").toLowerCase(Locale.ROOT);
    }
    public void addChildren(Category c){
        if(c==null) return;
        if(this.children == null) this.children = new ArrayList<>();
        this.children.add(c);
    }
    public void setProducts(Product product) {
        if(product == null) return;
        this.products.add(product);
    }
}
