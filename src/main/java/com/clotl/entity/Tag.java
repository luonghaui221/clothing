package com.clotl.entity;

import com.clotl.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.*;

@Table(name = "tag")
@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotEmpty(message = "Tag name cannot be empty")
    private String name;

    @Column(name = "code")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String code;

    @ManyToMany(mappedBy = "tags")
    @JsonIgnore
    private List<Product> products = new ArrayList<>();

    @PrePersist
    @PreUpdate
    private void generateCode(){
        if(this.name != null){
            this.code =  Objects.requireNonNull(Utils.removeAccents(this.name)).
                    replaceAll(" ","-").toLowerCase();
        }
    }
}
