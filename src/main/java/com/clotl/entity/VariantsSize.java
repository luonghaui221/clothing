package com.clotl.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;

@Entity
@Table(name = "variants_size")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VariantsSize {
    @EmbeddedId
    private VariantsSizeId id;

    @Valid
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @MapsId("variantsId")
    @JsonBackReference
    private Variants variants;

    @Valid
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @MapsId("sizeCode")
    private Size size;


    @Column(name = "quantity")
    @Min(value = 0,message = "quantity cannot be less than 0")
    private Integer quantity;

}
