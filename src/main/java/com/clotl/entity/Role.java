package com.clotl.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "roles")
public class Role {
    @Id
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;

    @ManyToMany(mappedBy = "roles")
    private List<Account> accounts;
}
