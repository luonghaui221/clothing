package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Table(name = "Account")
@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Account {
    @Id
    private String id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone")
    private String phone;

    @Column(name = "gender")
    private Boolean gender;

    @Column(name = "address")
    private String address;

    @Column(name = "locked")
    @JsonIgnore
    private boolean locked;


    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<CartItem> cartItems;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "users_roles",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id") }
    )
    private List<Role> roles;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<Order> orders;

    @OneToMany(mappedBy = "account",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    List<UserReviews> reviews;

    public String getName(){
        return String.format("%s %s",firstName,lastName);
    }
}
