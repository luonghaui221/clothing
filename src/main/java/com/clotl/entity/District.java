package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "district")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class District implements Serializable {
    @Id
    private String id;
    private String name;
    private String code;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "city_code", referencedColumnName = "code")
    @JsonIgnore
    private City city;

    @OneToMany(mappedBy = "district",fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Commune> communes;
}
