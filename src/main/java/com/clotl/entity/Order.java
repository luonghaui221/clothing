package com.clotl.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "`order`")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Order {
    @Id
    private String id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "address")
    private String address;
    @Column(name = "address_details")
    private String addressDetails;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "status")
    private String statusValue;
    @Transient
    private OrderStatus status;
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;
    @Column(name = "note")
    private String note;
    @Column(name = "total_price")
    private double totalPrice;
    @Column(name = "sale_price")
    private double salePrice;
    @Column(name = "delivery_cost")
    private double deliveryCost;
    @Column(name = "create_at")
    @CreatedDate
    private Timestamp createAt;
    @Column(name = "update_at")
    @LastModifiedDate
    private Timestamp updateAt;

    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private Account account;

    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    private Set<OrderItem> orderItems;

    @PostLoad
    void fillTransient() {
        if (!statusValue.isBlank()) {
            this.status = OrderStatus.of(statusValue);
        }
    }
}
