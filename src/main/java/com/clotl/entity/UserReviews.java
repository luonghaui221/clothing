package com.clotl.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;

@Entity
@Table(name = "review")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class UserReviews {
    @EmbeddedId
    UserReviewId id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @MapsId("userId")
    Account account;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @MapsId("productId")
    Product product;

    @Column(name = "content")
    private String content;
    @Column(name = "create_at")
    @CreatedDate
    private Date createAt;
    @Column(name = "update_at")
    @LastModifiedDate
    private Date updateAt;
}
