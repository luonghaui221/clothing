package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "size")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Size {
    @Id
    private String code;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "size", cascade = CascadeType.ALL,fetch = FetchType.LAZY,orphanRemoval = true)
    @Setter(AccessLevel.NONE)
    private List<VariantsSize> variants;

    public void setVariants(List<VariantsSize> v) {
        if(v == null) return;
        v.forEach(x -> x.setSize(this));
        this.variants = v;
    }
    public void clearVariant(){
        this.variants = null;
    }
    public void addVariants(VariantsSize vs){
        if(vs == null) return;
        variants.add(vs);
    }
}
