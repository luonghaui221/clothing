package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "city")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class City implements Serializable {
    @Id
    private String id;
    private String name;
    private String code;

    @OneToMany(mappedBy = "city",orphanRemoval = true,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<District> districts;
}
