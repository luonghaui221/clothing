package com.clotl.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cart_item")
public class CartItem {

    @EmbeddedId
    CartItemId id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    Account account;

    @ManyToOne
    @MapsId("variantsId")
    @JoinColumn(name = "variants_id")
    Variants variants;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "size")
    private String size;
}
