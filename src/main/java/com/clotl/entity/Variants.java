package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "variants")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Variants {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "color")
    private String color;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY,optional = false,cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    @Valid
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "variants", cascade= CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<VariantsSize> sizes;

    @Valid
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "variant", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Image> images;


    @OneToMany(mappedBy = "variants")
    private List<CartItem> cartItems;

    @OneToMany(mappedBy = "variants")
    private List<OrderItem> orderItems;

    public Variants(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addSize(VariantsSize vs){
        if(vs == null) return;
        this.sizes.add(vs);
        vs.setVariants(this);
    }

    public void setImages(List<Image> images) {
        for(Image i : images){
            i.setVariant(this);
        }
        this.images = images;
    }
    public void updateImages(List<Image> images){
        if(images == null) return;
        this.images.clear();
        for(Image i : images){
            this.images.add(i);
            i.setVariant(this);
        }
    }
    public void setSizes(List<VariantsSize> sizes) {
        if(sizes == null) return;
        for(VariantsSize i : sizes){
            i.setVariants(this);
        }
        this.sizes = sizes;
    }

    public void addImage(Image image){
        image.setVariant(this);
        this.images.add(image);
    }
    public void addImage(List<Image> images){
        for(Image i : images){
            addImage(i);
        }
    }

    public boolean isDuplicate(Variants v){
        return this.id.equals(v.getId());
    }
}
