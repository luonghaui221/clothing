package com.clotl.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

@Entity
@Table(name = "order_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem {
    @EmbeddedId
    OrderItemId id;

    @ManyToOne
    @MapsId("orderId")
    @Setter(AccessLevel.NONE)
    Order order;

    @ManyToOne
    @MapsId("variantsId")
    @Setter(AccessLevel.NONE)
    Variants variants;

    @Column(name = "quantity")
    private Integer quantity;


    @Column(name = "size")
    private String size;

    public void setOrder(Order order) {
        if(order == null) return;
        this.order = order;
    }

    public void setVariants(Variants variants) {
        if(variants == null) return;
        this.variants = variants;
        variants.getOrderItems().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return id.equals(orderItem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
