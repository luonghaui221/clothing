package com.clotl.entity;

public enum ImageType {
    thumbnail,
    content
}
