package com.clotl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "Product")
@Entity
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @Column(name = "sale_price")
    private Integer salePrice;

    @Column(name = "description")
    private String description;

    @Column(name = "sale_to")
    private Date saleTo;

    @Column(name = "is_deleted")
    @JsonIgnore
    private boolean isDeleted;

    @JsonIgnore
    @CreatedDate
    @Column(name = "create_at")
    private Date createAt;

    @JsonIgnore
    @LastModifiedDate
    @Column(name = "update_at")
    private Date updateAt;

    @Valid
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Variants> variants;

    @Valid
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "category_product",joinColumns = @JoinColumn(name = "product_id"),inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    @Valid
    @ManyToMany(cascade = CascadeType.ALL)
    //@Setter(AccessLevel.NONE)
    @JoinTable(name = "tag_product",joinColumns = @JoinColumn(name = "product_id"),inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;


    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
    //@Setter(AccessLevel.NONE)
    List<UserReviews> reviews;

    @ManyToMany(mappedBy = "products",cascade = CascadeType.ALL)
    //@Setter(AccessLevel.NONE)
    private List<Collection> collections;

    public Product(String name, Integer price, String description, Date createAt, Date updateAt) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public Product(String name, Integer price, String description, List<Variants> variants, List<Category> categories, List<Tag> tags) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.setVariants(variants);
        this.addCategory(categories);
        this.setTags(tags);
    }

    public void setVariants(List<Variants> variants) {
        if(variants == null) return;
        for(Variants v : variants){
            v.setProduct(this);
        }
        this.variants = variants;
    }
    public void addVariant(Variants v){
        if(v == null) return;
        variants.add(v);
        v.setProduct(this);
    }
    public void addListVariant(List<Variants> variants){
        if(variants == null) return;
        for(Variants v : variants){
            addVariant(v);
        }
    }

    public void addCategory(Category c) {
        if(c == null) return;
        if(this.categories == null) this.categories = new ArrayList<>();
        this.categories.add(c);
        c.getProducts().add(this);
    }
    public void addCategory(List<Category> categories) {
        if(categories == null) return;
        for(Category c : categories){
            c.setProducts(this);
        }
        this.categories = categories;
    }
//    public void setTags(List<Variants> variants) {
//        if(variants != null){
//            for(Variants v : variants){
//                v.setProduct(this);
//            }
//        }
//        this.variants = variants;
//    }
//    public void setReviews(List<Variants> variants) {
//        if(variants != null){
//            for(Variants v : variants){
//                v.setProduct(this);
//            }
//        }
//        this.variants = variants;
//    }
//    public void setCollections(List<Variants> variants) {
//        if(variants != null){
//            for(Variants v : variants){
//                v.setProduct(this);
//            }
//        }
//        this.variants = variants;
//    }
}
