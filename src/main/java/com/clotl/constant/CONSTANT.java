package com.clotl.constant;

import java.util.Arrays;
import java.util.List;

public class CONSTANT {
    // user.dir is root folder of project
    public static final String IMG_DIR = System.getProperty("user.dir")+"\\Images";
    public static final List<String> MIME_IMG_SUPPORTED = Arrays.asList("image/jpg","image/jpeg","image/png","image/webp");
}
