package com.clotl.controllers;

import com.clotl.DTO.ResponsePagination;
import com.clotl.service.IDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {
    @Autowired
    private IDashboardService dashboardService;

    @GetMapping("/revenue")
    @PreAuthorize("hasAnyAuthority('admin','employee')")
    public double getRevenue(
            @RequestParam(name = "day",required = false) Integer day,
            @RequestParam(name = "month",required = false) Integer month,
            @RequestParam(name = "year",required = false) Integer year) {
        return dashboardService.getRevenue(day,month,year);
    }

    @GetMapping("/outOfStock")
    @PreAuthorize("hasAnyAuthority('admin','employee')")
    public ResponsePagination getProductOutOfStock(
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "minPrice",defaultValue = "0") Double minPrice,
            @RequestParam(name = "maxPrice",defaultValue = "999999999") Double maxPrice,
            @RequestParam(name = "categories", required = false) List<Long> categories,
            @RequestParam(name = "sizes",required = false) List<String> sizes,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
            @RequestParam(name = "orderBy",defaultValue = "id") String orderBy,
            @RequestParam(name = "orderDirection",defaultValue = "desc") String orderDirection) {
        return dashboardService.getProductOutOfStock(name,minPrice,maxPrice,categories,sizes,page,pageSize,orderBy,orderDirection);
    }
}
