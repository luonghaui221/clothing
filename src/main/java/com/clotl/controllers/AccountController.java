package com.clotl.controllers;

import com.clotl.DTO.AccountDTO;
import com.clotl.DTO.ResponsePagination;
import com.clotl.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    public AccountDTO registerAccount(@Valid @RequestBody AccountDTO account){
        return accountService.resgisterAccount(account);
    }

    @PostMapping("/admin/register")
    @PreAuthorize("hasAnyAuthority('admin')")
    @ResponseStatus(HttpStatus.OK)
    public AccountDTO createAccount(@Valid @RequestBody AccountDTO account){
        return accountService.createAccount(account);
    }

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority('admin')")
    @ResponseStatus(HttpStatus.OK)
    public ResponsePagination getListAccount(
        @RequestParam(name = "email",required = false) String email,
         @RequestParam(name = "keywords",required = false) String keywords,
         @RequestParam(name = "statuses",required = false) List<String> statuses,
         @RequestParam(name = "phoneNumber",required = false) String phoneNumber,
         @RequestParam(name = "page",defaultValue = "1") Integer page,
         @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
         @RequestParam(name = "orderBy",defaultValue = "id") String orderBy,
         @RequestParam(name = "orderDirection",defaultValue = "desc") String orderDirection){
        return accountService.getListAccount(email,keywords,statuses,phoneNumber,page,pageSize,orderBy,orderDirection);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('admin')")
    @ResponseStatus(HttpStatus.OK)
    public AccountDTO getById(@PathVariable(name = "id") String id){
        return accountService.getById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('admin')")
    @ResponseStatus(HttpStatus.OK)
    public AccountDTO updateAccount(@PathVariable(name = "id") String id,@Valid @RequestBody AccountDTO account){
        account.setId(id);
        return accountService.updateAccount(account);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('admin')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> lockedAccount(@PathVariable String id){
        accountService.deleteAccount(id);
        return ResponseEntity.ok().body("ok");
    }


}
