package com.clotl.controllers;

import com.clotl.DTO.ResponsePagination;
import com.clotl.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    @GetMapping("/province")
    @ResponseStatus(HttpStatus.OK)
    public ResponsePagination getCity(
            @RequestParam(name = "code",required = false) String code,
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "20") Integer pageSize
    ){
        return addressService.getAllCity(code,name,page,pageSize);
    }

    @GetMapping("/district")
    @ResponseStatus(HttpStatus.OK)
    public ResponsePagination getDistrict(
            @RequestParam(name = "code",required = false) String code,
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "provinceCode",required = false) String provinceCode,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "20") Integer pageSize
    ){
        return addressService.getAllDistrict(code,name,provinceCode,page,pageSize);
    }

    @GetMapping("/commune")
    @ResponseStatus(HttpStatus.OK)
    public ResponsePagination getCommune(
            @RequestParam(name = "code",required = false) String code,
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "districtCode",required = false) String districtCode,
            @RequestParam(name = "provinceCode",required = false) String provinceCode,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "20") Integer pageSize
    ){
        return addressService.getAllCommune(code,name,provinceCode,districtCode,page,pageSize);
    }
}
