package com.clotl.controllers;

import com.clotl.DTO.ProductDTO;
import com.clotl.DTO.ProductSlimDTO;
import com.clotl.DTO.ResponseObject;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Category;
import com.clotl.entity.Product;
import com.clotl.repository.CategoryRepository;
import com.clotl.repository.ProductRepository;
import com.clotl.service.ProductService;
import com.clotl.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    RecommendService recommendService;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ResponsePagination> getAll(
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "minPrice",defaultValue = "0") Double minPrice,
            @RequestParam(name = "maxPrice",defaultValue = "999999999") Double maxPrice,
            @RequestParam(name = "categories", required = false) List<Long> categories,
            @RequestParam(name = "sizes",required = false) List<String> sizes,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
            @RequestParam(name = "orderBy",defaultValue = "id") String orderBy,
            @RequestParam(name = "orderDirection",defaultValue = "desc") String orderDirection) {
        return ResponseEntity.ok(productService.getAll(name,minPrice,maxPrice,categories,sizes,page,pageSize,orderBy,orderDirection));
    }

    @GetMapping("/ids")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductSlimDTO>> getInList(@RequestParam(name = "ids",required = false) List<Long> ids) {
        return ResponseEntity.ok(productService.getAllInIds(ids));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDTO getById(@PathVariable(name = "id") Long id){
        return productService.getById(id);
    }
    @PostMapping(value = "",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> addProduct(@Valid @RequestBody ProductDTO product) {
        ProductDTO result = productService.save(product);
        return result == null ?
                ResponseEntity.badRequest().body(null):
                ResponseEntity.ok().body(result);
    }

    @PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long id,@Valid @RequestBody ProductDTO product) {
        product.setId(id);
        ProductDTO result = productService.update(product);
        return result == null ?
                ResponseEntity.badRequest().body(null):
                ResponseEntity.ok().body(result);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<String> deleteProduct(@PathVariable Long id) {
        productService.delete(id);
        return ResponseEntity.ok().body("delete success");
    }
    @DeleteMapping(value = "/force/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<String> forceDeleteProduct(@PathVariable Long id) {
        productService.forceDelete(id);
        return ResponseEntity.ok().body("delete success");
    }

    @Transactional
    public void test(){
        String key1 = "áo";
        String key2 = "";
        String key3 = "";
        String NotKey1 = "mũ";
        String NotKey2 = "tất";
        List<Long> pids = Arrays.asList();
        List<Product> products = productRepository.findAll((root, query, cb) -> cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.like(root.get("name"), "%" + key1 + "%"),
                cb.like(root.get("name"), "%" + key2 + "%"),
                cb.like(root.get("name"), "%" + key3 + "%"),
                NotKey1.isBlank() ? cb.isTrue(cb.literal(true)) : cb.notLike(root.get("name"), "%" + NotKey1 + "%"),
                NotKey2.isBlank() ? cb.isTrue(cb.literal(true)) : cb.notLike(root.get("name"), "%" + NotKey2 + "%"),
                pids.size() > 0 ? root.get("id").in(pids) : cb.isTrue(cb.literal(true))
        ));
        List<Long> ids = Arrays.asList(1373L);
        List<Category> c = categoryRepository.findAllById(ids);
        products.forEach(p -> {
            List<Category> categories = new ArrayList<>();
            c.forEach(p::addCategory);
        });
        productRepository.saveAll(products);
    }

    @GetMapping("/recommendations/{id}")
    public ResponseObject recommendProduct(@PathVariable(name = "id") String id,
                                           @RequestParam(name = "k",required = false,defaultValue = "3") int kNearest){
        return recommendService.getRecommendations(id,kNearest);
    }
}
