package com.clotl.controllers;

import com.clotl.DTO.CategoryDTO;
import com.clotl.DTO.CategoryTreeData;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Category;
import com.clotl.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import javax.validation.Valid;
import java.sql.DriverManager;
import java.util.List;

@RestController
@RequestMapping("/api/category")
@Validated
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("")
    public ResponseEntity<List<CategoryTreeData>> findAll(){
        List<CategoryTreeData> result = categoryService.getAllCategory();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/list")
    public ResponseEntity<ResponsePagination> findAll(
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize
    ){
        ResponsePagination result = categoryService.getListCategory(page,pageSize);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CategoryDTO> findById(@PathVariable("id")Long id) {
        CategoryDTO result = categoryService.getCategoryById(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE},produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryDTO> addCategory(@Valid @RequestBody CategoryDTO category) {
        CategoryDTO result = categoryService.addCategories(category);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CategoryDTO> updateCategory(@PathVariable("id")Long id, @Valid @RequestBody CategoryDTO category){
        category.setCategoryId(id);
        CategoryDTO result = categoryService.updateCategories(category);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategories(@PathVariable("id")Long id){
        categoryService.deleteCategories(id);
        return ResponseEntity.ok().body("deleted");
    }
}
