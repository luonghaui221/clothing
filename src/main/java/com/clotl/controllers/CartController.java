package com.clotl.controllers;

import com.clotl.DTO.CartItemDTO;
import com.clotl.DTO.CartVariantsInfo;
import com.clotl.entity.CartItem;
import com.clotl.security.CustomUserDetails;
import com.clotl.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController{
    @Autowired
    CartService cartService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<CartVariantsInfo> getListCartItem(){
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return cartService.getListCart(userDetails.getAccount().getId());
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    public CartItemDTO addToCart(@Valid @RequestBody CartItemDTO cart){
        return cartService.addToCart(cart);
    }

    @PostMapping(value = "/all",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<CartItemDTO> addToCart(@NotNull @RequestBody List<@Valid CartItemDTO> cart){
        return cartService.addAllToCart(cart);
    }

    @DeleteMapping("")
    @ResponseStatus(HttpStatus.OK)
    public void removeFromCart(@RequestBody CartItemDTO cart){
        cartService.removeFromCart(cart);
    }

    @PutMapping("")
    @ResponseStatus(HttpStatus.OK)
    public CartItemDTO updateQuantity(@Valid @RequestBody CartItemDTO cart){
        return cartService.updateCart(cart);
    }
}
