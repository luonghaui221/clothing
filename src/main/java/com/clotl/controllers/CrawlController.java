package com.clotl.controllers;

import com.clotl.entity.Product;
import com.clotl.service.CrawlDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;



@Controller
public class CrawlController {
    @Autowired
    CrawlDataService crawlDataService;
    public ResponseEntity<String> crawlProduct(){
        List<Product> results = crawlDataService.crawlDataProduct();
        return results.isEmpty() ? ResponseEntity.ok().body("success") : ResponseEntity.badRequest().body("fail");
    }
    public ResponseEntity<String> crawlImage(){
        crawlDataService.crawlImage();
        return ResponseEntity.ok().body("success");
    }
}
