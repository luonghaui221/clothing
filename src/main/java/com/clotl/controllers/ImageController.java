package com.clotl.controllers;

import com.clotl.entity.Image;
import com.clotl.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/images")
public class ImageController {
    @Autowired
    ImageService imageService;
    @GetMapping("")
    public ResponseEntity<List<Image>> getAll(@RequestParam("variantId")String variantId){
        return ResponseEntity.ok().body(imageService.findAllByVariantID(variantId.trim()));
    }

    @GetMapping(value = "/{name:.+\\..+}",produces = {MediaType.IMAGE_JPEG_VALUE})
    public ResponseEntity<byte[]> getImage(@PathVariable("name")String name) throws IOException {
        return ResponseEntity.ok().body(imageService.findByName(name));
    }

    @PostMapping(path = "/upload",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<List<String>> uploadFile(@RequestParam("file")MultipartFile[] file) throws IOException {
        List<String> paths = imageService.saveImages(file);
        return paths.isEmpty() ?
                ResponseEntity.badRequest().body(paths):
                ResponseEntity.ok().body(paths);
    }
}
