package com.clotl.controllers;

import com.clotl.DTO.ResponseObject;
import com.clotl.DTO.ResponsePagination;
import com.clotl.DTO.ReviewDTO;
import com.clotl.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/review")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @GetMapping("/{id}")
    public ResponsePagination getAllByProductId(
            @PathVariable(name = "id") Long id,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize
    ){
        return reviewService.getReviewOfProduct(id,page,pageSize);
    }

    @PostMapping
    public ResponseObject addOrUpdateReview(@Valid @RequestBody ReviewDTO reviewDTO){
        return reviewService.createOrUpdateReview(reviewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseObject deleteReview(@PathVariable(name = "id")Long productId){
        return reviewService.deleteReview(productId);
    }
}
