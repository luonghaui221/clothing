package com.clotl.controllers;

import com.clotl.DTO.OrderDTO;
import com.clotl.DTO.OrderInfo;
import com.clotl.DTO.ResponseObject;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.OrderStatus;
import com.clotl.service.OrderService;
import com.clotl.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('admin','employee')")
    public ResponsePagination getListOrder(
        @RequestParam(name = "email",required = false) String email,
        @RequestParam(name = "keywords",required = false) String keywords,
        @RequestParam(name = "statuses",required = false) List<String> statuses,
        @RequestParam(name = "phoneNumber",required = false) String phoneNumber,
        @RequestParam(name = "fromDate",defaultValue = "1970/01/01") Date fromDate,
        @RequestParam(name = "toDate",defaultValue = "#{new java.util.Date()}") Date toDate,
        @RequestParam(name = "page",defaultValue = "1") Integer page,
        @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
        @RequestParam(name = "orderBy",defaultValue = "createAt") String orderBy,
        @RequestParam(name = "orderDirection",defaultValue = "desc") String orderDirection) {
        return orderService.getListOrder(email,keywords,statuses,phoneNumber,fromDate,toDate,page,pageSize,orderBy,orderDirection);
    }

    @GetMapping
    public ResponsePagination getListOrderByEmail(
            @RequestParam(name = "keywords",required = false) String keywords,
            @RequestParam(name = "statuses",required = false) List<String> statuses,
            @RequestParam(name = "fromDate",defaultValue = "1970/01/01") Date fromDate,
            @RequestParam(name = "toDate",defaultValue = "#{new java.util.Date()}") Date toDate,
            @RequestParam(name = "page",defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
            @RequestParam(name = "orderBy",defaultValue = "createAt") String orderBy,
            @RequestParam(name = "orderDirection",defaultValue = "desc") String orderDirection) {
        return orderService.getListOrder(Utils.getCurrentUser().getUsername(),keywords,statuses,null,fromDate,toDate,page,pageSize,orderBy,orderDirection);
    }

    @GetMapping("/{id}")
    public OrderDTO getOrderById(@PathVariable String id){
        return orderService.getOderById(id);
    }

    @PostMapping
    public OrderDTO createOrder(@Valid @RequestBody OrderDTO orderDTO){
        return orderService.create(orderDTO);
    }

    @PutMapping("/{id}")
    public OrderDTO updateOrder(@PathVariable(name = "id") String id, @RequestBody @Valid OrderInfo orderDTO){
        orderDTO.setId(id);
        return orderService.update(orderDTO);
    }

    @PutMapping("/confirm/{id}")
    public ResponseObject confirm(@PathVariable(name = "id") String id){
        orderService.confirm(id);
        return new ResponseObject(HttpStatus.OK.value(),"updated",null);
    }

    @PutMapping("/cancel/{id}")
    public ResponseObject cancel(@PathVariable(name = "id") String id){
        orderService.cancel(id);
        return new ResponseObject(HttpStatus.OK.value(),"canceled",null);
    }
}
