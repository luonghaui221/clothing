package com.clotl.controllers;

import com.clotl.DTO.RequestPagination;
import com.clotl.DTO.ResponsePagination;
import com.clotl.entity.Tag;
import com.clotl.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    @GetMapping("")
    public ResponseEntity<ResponsePagination> findAll(@Valid @RequestBody(required = false) RequestPagination pagination){
        if(pagination == null) pagination = new RequestPagination();
        Page<Tag> result = tagService.findAll(pagination);
        return ResponseEntity.ok().body(new ResponsePagination(
                result.getContent(),
                result.getPageable().getPageNumber()+1,
                result.getTotalPages(),
                (int) result.getTotalElements(),
                result.getSize()
        ));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("")
    public ResponseEntity<Tag> addTag(@Valid @RequestBody Tag tag){
        Tag result = tagService.addTag(tag);
        return result != null ? ResponseEntity.ok().body(result):
                ResponseEntity.internalServerError().body(result);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<Tag> updateTag(
            @PathVariable("id")Long id,
            @RequestBody Tag tag){
        tag.setId(id);
        Tag result = tagService.updateTag(tag);
        return result != null ? ResponseEntity.ok().body(result):
                ResponseEntity.internalServerError().body(result);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTag(@PathVariable("id")Long id){
        tagService.deleteTag(id);
        return  ResponseEntity.ok().body(String.format("Size code: %s is deleted",id));
    }
}
