package com.clotl.controllers;

import com.clotl.entity.Size;
import com.clotl.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/size")
public class SizeController {
    @Autowired
    private SizeService sizeService;

    @GetMapping("")
    public ResponseEntity<List<Size>> findAll(){
        return ResponseEntity.ok().body(sizeService.findAll());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("")
    public ResponseEntity<Size> addSize(@Valid @RequestBody Size size){
        Size result = sizeService.addSize(size);
        return result != null ? ResponseEntity.ok().body(result):
                ResponseEntity.internalServerError().body(result);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{code}")
    public ResponseEntity<Size> updateSize(
            @PathVariable("code")String code,
            @RequestBody Size size){
        size.setCode(code);
        Size result = sizeService.updateSize(size);
        return result != null ? ResponseEntity.ok().body(result):
                ResponseEntity.internalServerError().body(result);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{code}")
    public ResponseEntity<String> deleteSize(@PathVariable("code")String code){
        sizeService.deleteSize(code);
        return  ResponseEntity.ok().body(String.format("Size code: %s is deleted",code));
    }
}
