package com.clotl.controllers;

import com.clotl.entity.Role;
import com.clotl.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority('admin')")
    @Transactional(readOnly = true)
    public List<Role> getListRole(){
        List<Role> roles = roleRepository.findAll();
        roles.forEach(x -> x.setAccounts(null));
        return roles;
    }
}
