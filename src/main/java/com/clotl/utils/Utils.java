package com.clotl.utils;


import com.clotl.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.util.Base64;

public class Utils {
    public static String escapeHtmlSpecialCharacter(String text){
        return text == null ? null : text.replaceAll("&", "&amp;")
            .replaceAll("<", "&lt;")
            .replaceAll(">", "&gt;")
            .replaceAll("\"", "&quot;")
            .replaceAll("'", "&#39;");
    }

    public static String removeAccents(String text){
        String normal =  Normalizer.normalize(text, Normalizer.Form.NFKD);
        return normal.replaceAll("\\p{M}","");
    }
    public static CustomUserDetails getCurrentUser(){
        Object o = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return o instanceof String ? null : (CustomUserDetails) o;
    }
    public static String generateString(int length){
        SecureRandom random = new SecureRandom();

        // Use a random 8-byte array as the input to the hash function
        byte[] input = new byte[8];
        random.nextBytes(input);

        // Use a cryptographic hash function to generate a 16-byte hash of the input
        byte[] hash = hash(input);

        // Convert the hash to a base64-encoded string and take the first 12 characters
        return Base64.getUrlEncoder()
                .withoutPadding()
                .encodeToString(hash)
                .replaceAll("[^a-zA-Z0-9]","")
                .toUpperCase()
                .substring(0, length);
    }

    private static byte[] hash(byte[] input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            return md.digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error hashing input", e);
        }
    }
}
