package com.clotl.utils;

import org.springframework.stereotype.Component;

@Component
public class SnowFlakeIdGenerator {

    private static final int SEQUENCE_BITS = 12;
    private static final int WORKER_ID_BITS = 5;
    private static final int DATACENTER_ID_BITS = 5;

    private static final int MAX_SEQUENCE = (1 << SEQUENCE_BITS) - 1;
    private static final int MAX_WORKER_ID = (1 << WORKER_ID_BITS) - 1;
    private static final int MAX_DATACENTER_ID = (1 << DATACENTER_ID_BITS) - 1;

    private static final long EPOCH = 1288834974657L; // 2006-03-21T20:50:14.657Z

    private final int workerId;
    private final int datacenterId;
    private long lastTimestamp = -1L;
    private int sequence = 0;

    public SnowFlakeIdGenerator(){
        this(1,1);
    }

    public SnowFlakeIdGenerator(int workerId, int datacenterId) {
        if (workerId > MAX_WORKER_ID || workerId < 0) {
            throw new IllegalArgumentException(String.format("Worker ID must be between 0 and %d", MAX_WORKER_ID));
        }
        if (datacenterId > MAX_DATACENTER_ID || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("Datacenter ID must be between 0 and %d", MAX_DATACENTER_ID));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    public synchronized long getNextId() {
        long timestamp = System.currentTimeMillis();
        if (timestamp < lastTimestamp) {
            throw new RuntimeException("Clock moved backwards. Refusing to generate ID.");
        }
        if (timestamp == lastTimestamp) {
            sequence = (sequence + 1) & MAX_SEQUENCE;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0;
        }
        lastTimestamp = timestamp;
        return ((timestamp - EPOCH) << (WORKER_ID_BITS + SEQUENCE_BITS))
                | ((long) datacenterId << SEQUENCE_BITS)
                | ((long) workerId << SEQUENCE_BITS)
                | sequence;
    }

    private long tilNextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }
}
