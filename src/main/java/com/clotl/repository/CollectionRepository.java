package com.clotl.repository;

import com.clotl.entity.Category;
import com.clotl.entity.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CollectionRepository extends JpaRepository<Collection,Long> {
    @Query("select c.id from Collection c where c.id in :ids")
    Set<Long> getAllIdIn(@Param("ids") java.util.Collection<Long> ids);

    List<Collection> getAllByIdIn(java.util.Collection<Long> ids);
}
