package com.clotl.repository;

import com.clotl.entity.Category;
import com.clotl.entity.Size;
import com.clotl.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.Collection;
import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag,Long> {
    List<Tag> getAllByIdIn(Collection<Long> ids);
}
