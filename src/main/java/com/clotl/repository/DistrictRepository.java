package com.clotl.repository;

import com.clotl.entity.City;
import com.clotl.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DistrictRepository extends JpaRepository<District,String>, JpaSpecificationExecutor<District> {
    Optional<District> findByCode(String s);

    @Query("select d from District d where d.city.code = :code")
    Optional<District> findAllByCityCode(@Param("code") String cityCode);
}
