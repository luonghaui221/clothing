package com.clotl.repository;

import com.clotl.entity.UserReviewId;
import com.clotl.entity.UserReviews;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewRepository extends JpaRepository<UserReviews, UserReviewId> {

    @Query("select r from UserReviews r where r.product.id = :id")
    Page<UserReviews> getAllByProductId(@Param("id") Long id, Pageable pagination);

    @Query("select case when count(r) > 0 then true else false end " +
            "from UserReviews r where r.account.id = :userId and r.product.id = :productId")
    boolean existByUserIdAndProductId(String userId,Long productId);

    @Query("select r from UserReviews r where r.account.id = :userId and r.product.id = :productId")
    Optional<UserReviews> findByUserIdAndProductId(@Param("userId") String userId,@Param("productId")Long productId);
}
