package com.clotl.repository;

import com.clotl.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account,String>, JpaSpecificationExecutor<Account> {
    Optional<Account> findUserByEmailAndPassword(String email, String password);
    Optional<Account> findAccountByEmail(String email);
    boolean existsAccountByEmail(String email);
}
