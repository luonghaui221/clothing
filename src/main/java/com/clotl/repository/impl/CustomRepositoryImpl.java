package com.clotl.repository.impl;

import com.clotl.repository.CustomRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.html.parser.Entity;

@Repository
public class CustomRepositoryImpl implements CustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void detach(Object entity) {
        em.detach(entity);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
