package com.clotl.repository;

import com.clotl.entity.VariantsSize;
import com.clotl.entity.VariantsSizeId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface VariantSizeRepository extends JpaRepository<VariantsSize, VariantsSizeId> {
    @Modifying
    @Query("delete from VariantsSize vs where vs.id.sizeCode = :sizeCode and vs.id.variantsId = :variantsId")
    void deleteById(@Param("sizeCode") String sizeCode,@Param("variantsId") String variantsId);

    @Modifying
    @Query("delete from VariantsSize vs where vs.id.variantsId = :vid")
    void deleteAllByVariantsId(@Param("vid") String variantsId);

    @Query("select vs from VariantsSize vs where vs.variants.id = :variantId and vs.size.code = :sizeCode")
    Optional<VariantsSize> findById(@Param("variantId") String variantId, @Param("sizeCode") String sizeCode);
}
