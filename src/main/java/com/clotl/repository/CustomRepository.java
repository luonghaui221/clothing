package com.clotl.repository;

import javax.persistence.EntityManager;
import javax.swing.text.html.parser.Entity;

public interface CustomRepository {
    void detach(Object entity);
    EntityManager getEntityManager();
}
