package com.clotl.repository;

import com.clotl.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image,String> {
//    @Modifying
//    @Query("insert into Image(id,variants_id,type) select :id,:variants_id,:type")
//    @Transactional
//    List<Image> addImage(@Param("id")String id, @Param("variants_id") String variants_id, @Param("type")ImageType type);

    List<Image> findAllByVariantId(String id);

    void deleteAllById(String id);
}
