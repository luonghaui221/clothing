package com.clotl.repository;

import com.clotl.entity.Commune;
import com.clotl.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommuneRepository extends JpaRepository<Commune,String>, JpaSpecificationExecutor<Commune> {
    Optional<Commune> findByCode(String s);

    @Query("select c from Commune c where c.district.code = :code")
    Optional<Commune> findAllByDistrictCode(@Param("code") String districtCode);
}
