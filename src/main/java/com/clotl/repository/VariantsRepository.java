package com.clotl.repository;

import com.clotl.entity.Variants;
import org.aspectj.weaver.ast.Var;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

@Repository
public interface VariantsRepository extends JpaRepository<Variants,String> {
    @Query(value = "select * from product_variants limit :limit offset :offset",nativeQuery = true)
    List<Variants> findLimit(@Param("limit")Integer limit,@Param("offset")Integer offset);

    @Transactional(readOnly = true,noRollbackFor = RuntimeException.class,propagation = Propagation.REQUIRES_NEW)
    Optional<Variants> findTopByOrderByIdDesc();
}
