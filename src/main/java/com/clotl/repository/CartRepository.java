package com.clotl.repository;

import com.clotl.entity.CartItem;
import com.clotl.entity.CartItemId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<CartItem, CartItemId> {
    @Query("select c from CartItem c where c.id.userId = :userId")
    List<CartItem> findAllByUserId(@Param("userId") String id);

    @Query("select c from CartItem c where c.account.email = :email")
    List<CartItem> findAllByEmail(@Param("email") String email);

    @Query("select c from CartItem c where c.id.userId = :userId and c.id.variantsId = :variantsId")
    Optional<CartItem> findByUserIdAndVariantsId(@Param("userId") String userId,@Param("variantsId") String variantsId);

}
