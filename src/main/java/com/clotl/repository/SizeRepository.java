package com.clotl.repository;

import com.clotl.entity.Size;
import com.clotl.entity.Variants;
import com.clotl.mapper.SizeIdMapper;
import com.clotl.mapper.SizeIdToEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SizeRepository extends JpaRepository<Size,String> {

    @Query("select s from Size s where s.code in :ids")
    List<Size> findAllByCodeIn(@Param("ids") Collection<String> ids);

    @Transactional(readOnly = true)
    @Query("select s from Size s left join fetch s.variants where s.code = :code")
    Optional<Size> findByCode(@Param("code") String code);

}
