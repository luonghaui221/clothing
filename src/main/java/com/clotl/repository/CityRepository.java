package com.clotl.repository;

import com.clotl.entity.City;
import com.clotl.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City,String>, JpaSpecificationExecutor<City> {
    Optional<City> findByCode(String s);
}
