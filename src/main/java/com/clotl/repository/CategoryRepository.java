package com.clotl.repository;

import com.clotl.entity.Category;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long>, JpaSpecificationExecutor<Category> {

    @Query("SELECT c FROM Category c WHERE c.parent.id IS NULL")
    List<Category> findRootCategories();

    @Query("SELECT c FROM Category c WHERE c.parent.id = :parentId")
    List<Category> findChildCategories(@Param("parentId") Long parentId);

    List<Category> getAllByIdIn(Collection<Long> ids);

    default List<Category> findAllAsTreeData(){
        Map<Long,Category> all = findAll().stream().collect(Collectors.toMap(Category::getId, map -> {
            map.setChildren(null);
            return map;
        }));
        all.forEach((key,value) -> {
            if(value.getParent() != null && all.get(value.getParent().getId()) != null){
                all.get(value.getParent().getId()).addChildren(value);
            }
        });
        return all.values().stream().filter(category -> category.getParent() == null).collect(Collectors.toList());
    }
}
