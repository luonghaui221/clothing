package com.clotl.repository;

import com.clotl.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, String>, JpaSpecificationExecutor<Order> {
    Optional<Order> findById(String id);

    @Query("select o from Order o where o.account.email = :email")
    List<Order> findAllByEmail(@Param("email") String email);

    @Query("SELECT CASE WHEN COUNT(o) > 0 THEN true ELSE false END " +
            "FROM Order o left join o.account ac left join o.orderItems oi left join oi.variants v left join v.product p " +
            "where p.id = :productId and ac.email = :email")
    boolean isBought(@Param("productId") Long productId,@Param("email") String email);
}
