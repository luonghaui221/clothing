package com.clotl.DTO;

import com.clotl.entity.Size;
import com.clotl.entity.Variants;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class VariantsSizeDTO {
    @NotEmpty(message = "Size code cannot be empty")
    private String sizeCode;
    @Min(value = 0,message = "quantity must be greater than or equal 0")
    @Max(value = 999999999,message = "quantity must be less than 999999999")
    private Integer quantity;
}
