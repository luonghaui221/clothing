package com.clotl.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class CartVariantsInfo {
    private String variantsId;
    private String productName;
    private String variantsName;
    private Integer quantity;
    private String size;
    private String thumbnail;
    private Integer price;
    private Integer salePrice;
}
