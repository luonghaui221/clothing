package com.clotl.DTO;

import com.clotl.entity.Category;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
public class CategoryDTO {
    private Long categoryId;
    private String url;
    private String categoryName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long parentId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonBackReference
    private CategoryDTO parent;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonManagedReference
    private List<CategoryDTO> children;
}
