package com.clotl.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDTO {
    @NotEmpty(message = "Content cannot be empty!")
    private String content;
    @NotNull(message = "ProductId cannot be null")
    private Long productId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String userId;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String userName;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createAt;
}
