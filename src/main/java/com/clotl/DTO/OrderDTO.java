package com.clotl.DTO;

import com.clotl.entity.OrderStatus;
import com.clotl.entity.PaymentMethod;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
public class OrderDTO {
    private String id;
    @NotEmpty(message = "FirstName cannot be empty")
    private String firstName;
    @NotEmpty(message = "LastName cannot be empty")
    private String lastName;
    @NotEmpty(message = "Address cannot be empty")
    private String address;
    private String addressDetails;
    @NotEmpty(message = "Email cannot be empty")
    @Email(message = "Email invalid")
    private String email;
    @NotEmpty(message = "PhoneNumber cannot be empty")
    @Pattern(regexp = "^\\d{10,11}$", message = "PhoneNumber invalid")
    private String phoneNumber;
    @NotNull(message = "PaymentMethod cannot be empty")
    private PaymentMethod paymentMethod;
    private String note;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotEmpty(message = "Carts cannot be empty!")
    private List<CartItemDTO> carts;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private double totalPrice;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private double salePrice;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private double deliveryCost;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createAt;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String addressName;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private OrderStatus status;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String userId;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String createBy;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Set<OrderItemDTO> orderItems;
}
