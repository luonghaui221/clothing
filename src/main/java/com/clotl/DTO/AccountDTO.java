package com.clotl.DTO;

import com.clotl.entity.CartItem;
import com.clotl.entity.Order;
import com.clotl.entity.Role;
import com.clotl.entity.UserReviews;
import com.clotl.validation.DateValidation;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Validated
public class AccountDTO {
    private String id;
    @NotEmpty(message = "email cannot be empty")
    private String email;
    @NotEmpty(message = "Firstname cannot be empty")
    private String firstName;
    @NotEmpty(message = "Lastname cannot be empty")
    private String lastName;
    @NotNull(message = "birthday cannot be empty")
    @DateValidation(format = "yyyy/MM/dd",message = "Date format must be 'yyyy/MM/dd'")
    private String birthday;
    private boolean gender;
    @NotEmpty(message = "Address cannot be empty")
    private String address;
    @NotEmpty(message = "Phone cannot be empty")
    private String phone;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private boolean locked;
    private List<Integer> roleIds;
}
