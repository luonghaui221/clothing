package com.clotl.DTO;

import com.clotl.entity.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
public class VariantsDTO {
    private String id;
    @NotEmpty(message = "Variants name cannot be empty")
    private String name;
    @NotEmpty(message = "variants color cannot be empty")
    private String color;
    @NotEmpty(message = "thumbnail cannot be empty")
    private String thumbnail;
    private List<@Valid Image> images;
    @NotEmpty(message = "sizes cannot be empty")
    private List<@Valid VariantsSizeDTO> sizes;
}
