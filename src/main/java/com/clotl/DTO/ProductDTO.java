package com.clotl.DTO;

import com.clotl.entity.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
public class ProductDTO {
    private Long id;
    @NotEmpty(message = "Product name can not be empty")
    private String name;
    @Min(value = 0, message = "Price can not be less than 0")
    @Max(value = 100_000_000, message = "Price can not be more than 100,000,000")
    private Integer price;
    @Min(value = 0, message = "Sale price can not be less than 0")
    @Max(value = 100_000_000, message = "Sale price can not be more than 100,000,000")
    private Integer salePrice;
    private String description;
    private Date saleTo;
    @NotEmpty(message = "Variants cannot be empty")
    @NotNull(message = "Variants cannot be empty")
    private List<@Valid VariantsDTO> variants;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<CategoryDTO> categories;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Tag> tags;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int reviews;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Collection> collections;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    Set<Long> categoryIds;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    Set<Long> tagIds;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    Set<Long> collectionIds;
}
