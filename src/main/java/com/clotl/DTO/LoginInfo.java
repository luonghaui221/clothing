package com.clotl.DTO;

import lombok.Data;

@Data
public class LoginInfo {
    private String email;
    private String password;
}
