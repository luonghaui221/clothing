package com.clotl.DTO;

import lombok.Data;

@Data
public class RequestPagination {
    private Integer page = 1;
    private Integer size = 20;
}
