package com.clotl.DTO;

import lombok.Data;

import java.util.Date;
import java.util.List;
@Data
public class ProductSlimDTO {
    private Long id;
    private String name;
    private Integer price;
    private Integer salePrice;
    private String description;
    private Date saleTo;
    private List <String> imgs;
}
