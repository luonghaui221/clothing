package com.clotl.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDTO {
    private String id;
    private String productName;
    private String variantName;
    private Integer price;
    private Integer salePrice;
    private Date saleTo;
    private String thumbnail;
    private Integer quantity;
    private String size;
}
