package com.clotl.DTO;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CartItemDTO {
    @NotEmpty(message = "variantsId cannot be empty")
    private String variantsId;
    @NotNull
    @Min(value = 1, message = "quantity must be greater than 0")
    @Max(value = 999, message = "quantity must be less than 999")
    private Integer quantity;
    @NotEmpty(message = "size cannot be empty")
    private String size;
}
