package com.clotl.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryTreeData {
    private Long key;
    private Long value;
    private String title;
    private List<CategoryTreeData> children;
}
