package com.clotl.DTO;

import com.clotl.entity.PaymentMethod;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class OrderInfo {
    private String id;
    @NotEmpty(message = "FirstName cannot be empty")
    private String firstName;
    @NotEmpty(message = "LastName cannot be empty")
    private String lastName;
    @NotEmpty(message = "Address cannot be empty")
    private String address;
    private String addressDetails;
    @NotEmpty(message = "Email cannot be empty")
    @Email(message = "Email invalid")
    private String email;
    @NotEmpty(message = "PhoneNumber cannot be empty")
    @Pattern(regexp = "^\\d{10,11}$", message = "PhoneNumber invalid")
    private String phoneNumber;
    private String note;
}
