package com.clotl;

import com.clotl.controllers.ProductController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.clotl"},exclude = SecurityAutoConfiguration.class)
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.clotl.repository")
@EnableJpaAuditing
public class StoreApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(StoreApplication.class, args);
//        System.out.println("=================begin==================");
//        context.getBean(ProductController.class).test();
//        System.out.println("=================success==================");
    }
}
