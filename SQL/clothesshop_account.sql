insert into clothesshop.account (id, email, password, first_name, last_name, birthday, gender, address, locked, phone)
values  ('123', 'luonghaui221@gmail.com', '$2a$10$hyU8giIX64eKVbCp.8pYCeve1NyCGfEghoq0Pnrj0ymbpvME6lt1a', 'luong', 'nguyen', '2001-11-27 00:00:00', true, 'hanoi', false, ''),
        ('50999930199347200', '12', '12', '21', '21', '2001-11-27 00:00:00', false, '12', false, '12'),
        ('51000283815481344', '123', '$2a$10$93X06g4eBhsmrJ0C6RcU0e4sSbSSQ191HAzKfBR4UdGyIcxdpm6O2', '21', '21', '2001-11-27 00:00:00', false, '12', false, '12'),
        ('51000485390454784', 'luonghaui2211@gmail.com', '$2a$10$qvVWofP2.zcOY2JvCuYoveypIrVL3R47pokS4CCLXA4HgkW9q7fle', 'a', 'a', '2001-11-27 00:00:00', false, 'ha', false, '0975658029'),
        ('ADMIN001', 'luong221@administrator.com', '$2a$10$hyU8giIX64eKVbCp.8pYCeve1NyCGfEghoq0Pnrj0ymbpvME6lt1a', 'luong', 'nguyen', '2001-11-27 15:37:12', true, 'hanoi', false, '');