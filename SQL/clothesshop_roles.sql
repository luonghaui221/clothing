insert into clothesshop.roles (id, name, code)
values  (5, 'administrator', 'admin'),
        (6, 'employee', 'employee'),
        (7, 'customer', 'customer');