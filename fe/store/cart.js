import {
    getListCartItem,
    addToCart,
    removeFromCart,
    updateQuantity,
    addAllToCart
}
from "../services/cart";
import {findIndex} from 'lodash'

export const mutations = {
    saveCart(state, data) {
        state.cart = data
    },
    removeCart(state) {
        state.cart = []
    },
    addItemToCart(state,data){
        if(!Array.isArray(state.cart)){
            state.cart = []
        }
        const index = findIndex(state.cart,c => c.variantsId === data.variantsId && c.size === data.size)
        if(index >= 0){
            state.cart.splice(index,1,{...state.cart[index],quantity:state.cart[index].quantity + data.quantity})
        }else{
            state.cart.push(data)
        }
    },
    removeFromCart(state,data){
        const index = findIndex(state.cart,c => c.variantsId === data.variantsId && c.size === data.size)
        if(index >= 0){
            state.cart.splice(index,1)
        }
    },
    updateCartItem(state,data){
        const index = findIndex(state.cart,c => c.variantsId === data.variantsId && c.size === data.size)
        if(index >= 0){
            state.cart.splice(index,1,data)
        }
    },
    addToFavorite(state,data){
        state.favorite.push(data)
    },
    removeFavorite(state,data){
        state.favorite = state.favorite.filter(f => f !== data)
    }
}

export const actions = {
    async addItemToCart({commit,getters,rootGetters},payload) {
        if(rootGetters['auth/isAuthenticated']){
            const data = await addToCart({
                variantsId: payload.variantsId,
                size: payload.size,
                quantity: payload.quantity
            })
            commit('addItemToCart',payload)
            return data
        }else{
            commit('addItemToCart',payload)
        }
    },
    async removeFromCart({commit,rootGetters},payload){
        if(rootGetters['auth/isAuthenticated']){
            return removeFromCart(payload).then(resp => {
                commit('removeFromCart',payload)
            })
        }else{
            commit('removeFromCart',payload)
        }
    },
    async updateQuantity({commit,rootGetters},payload){
        if(rootGetters['auth/isAuthenticated']){
            return updateQuantity(payload).then(resp => {
                commit('updateCartItem',payload)
            })
        }else{
            commit('updateCartItem',payload)
        }
    },
    getListCart({commit,getters,rootGetters}){
        if(rootGetters['auth/isAuthenticated']){
            if(getters.getCart.length >= 0){
                addAllToCart(getters.getCart)
            }
            getListCartItem().then(resp => {
                commit('saveCart',resp)
            })
        }
    },
    clearCart({commit}){
        commit('removeCart')
    },
    addFavorite({commit},payload){
        commit('addToFavorite',payload)
    },
    removeFavorite({commit},payload){
        commit('removeFavorite',payload)
    }
}

export const state = () => ({
    cart: [],
    favorite: []
})

export const getters = {
    getCartSize(state,getters){
        return getters.getCart.length
    },
    getListFavoriteSize(state,getters){
        return getters.getFavorite.length
    },
    getCart(state){
        return state.cart
    },
    getFavorite(state){
        return state.favorite
    }
}
