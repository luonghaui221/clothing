import {findIndex} from 'lodash'

export const mutations = {
    addKeyword(state, keyword) {
        state.keywords.push(keyword)
    }
}

export const actions = {
    addKeyword({commit, dispatch}, keyword) {
        commit('addKeyword',keyword)
    }
}

export const state = () => ({
    keywords: [],
})

export const getters = {
    getListKeywords(state){
        return state.keywords
    },
    getLastKeyword: (state) => (size) => {
        size *= -1
        return keywords.slice(size)
    },
}
