// import {login} from "../services/account";
// import Cookie from 'js-cookie'
// import {JWT} from "../utils/jwtDecoder";
// export const mutations = {
//   saveAccount(state, data) {
//     state.account = data
//   },
//   removeAccount(state){
//     console.log('call remove')
//     state.account = {}
//     Cookie.remove('jwt')
//     Cookie.remove('type')
//     Cookie.remove('account')
//   },
// }
//
// export const actions = {
//   async loginAsync({commit, dispatch}, data) {
//     await login(data)
//         .then(resp => {
//           const accInfo = JWT.decode(resp.accessToken)
//           commit('saveAccount',accInfo)
//           dispatch('cart/getListCartItem', null, { root: true })
//         })
//   },
//   clearAccount({commit, dispatch}, data){
//     commit('removeAccount')
//   },
//   logout({commit, dispatch}, data) {
//     commit('removeAccount')
//     dispatch('cart/clearCart', null, { root: true })
//   },
// }
//
// export const state = () => ({
//   account:{},
// })
//
// export const getters = {
//   getAccount(state) {
//     return state.account
//   },
//   getAccountName(state){
//     return state.account?.name || ''
//   },
//   isLoggedIn(state){
//     return state.account !== null && Object.keys(state.account).length !== 0 &&
//         state.account.id !== undefined && state.account.id.length !== 0
//   },
//   isPermission: (state) => (role) => {
//     return typeof state.account.role.find(x => x === role) === 'string'
//   }
// }
