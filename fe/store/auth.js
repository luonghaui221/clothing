import {login} from "../services/account";
import {JWT} from "../utils/jwtUtils";
import Cookies from 'js-cookie'
// reusable aliases for mutations
export const AUTH_MUTATIONS = {
    SET_USER: 'SET_USER',
    SET_PAYLOAD: 'SET_PAYLOAD',
    LOGOUT: 'LOGOUT',
}

export const state = () => ({
    access_token: null, // JWT access token
    id: null, // user id
    email_address: null, // user email address
    name: null,
    role: null,
})

export const mutations = {
    // store the logged in user in the state
    [AUTH_MUTATIONS.SET_USER] (state, { id, email, name, role}) {
        state.id = id
        state.email_address = email
        state.name = name
        state.role = role
    },

    // store new or updated token fields in the state
    [AUTH_MUTATIONS.SET_PAYLOAD] (state, payload) {
        state.access_token = payload
    },

    // clear our the state, essentially logging out the user
    [AUTH_MUTATIONS.LOGOUT] (state) {
        state.id = null
        state.email_address = null
        state.access_token = null
        state.name = null
        state.role = null
    },
}

export const actions = {
    async login ({ commit, dispatch }, data) {
        // make an API call to login the user with an email address and password
        const resp = await login(data)
        const payload = resp.accessToken
        const accInfo = JWT.validateJwt(resp.accessToken,'')
        const user = {
            id: accInfo.id,
            name: accInfo.name,
            role: accInfo.role,
            email: accInfo.sub
        }
        // commit the user and tokens to the state
        commit(AUTH_MUTATIONS.SET_USER, user)
        commit(AUTH_MUTATIONS.SET_PAYLOAD, payload)

        dispatch('cart/getListCart', null, { root: true })
    },

    async register ({ commit }, { email_addr, password }) {
        // make an API call to register the user
        const { data: { data: { user, payload } } } = await this.$axios.post(
            '/api/auth/register',
            { email_address, password }
        )

        // commit the user and tokens to the state
        commit(AUTH_MUTATIONS.SET_USER, user)
        commit(AUTH_MUTATIONS.SET_PAYLOAD, payload)
    },

    // logout the user
    logout ({ commit, dispatch }) {
        commit(AUTH_MUTATIONS.LOGOUT)
        dispatch('cart/clearCart', null, { root: true })
    },
}

export const getters = {
    // determine if the user is authenticated based on the presence of the access token
    isAuthenticated: (state) => {
        return state.access_token && state.access_token !== ''
    },
    getName: (state) => {
        return state.name
    },
    getEmail: (state) => {
        return state.email_address
    },
    getAccessToken: (state) => {
        return state.access_token
    },
    getPermission: (state) => {
        return state.role
    }
}
