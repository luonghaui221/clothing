export const JWT = {
    decode: (token) => {
        try {
            return JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString())
        } catch (err) {
            return null
        }
    },
    validateJwt: (jwt, secret) =>
    {
        if(typeof jwt !== 'string'){
            throw new Error('Invalid JWT');
        }
        const [headerB64, payloadB64, signature] = jwt.split('.');
        const header = JSON.parse(Buffer.from(headerB64, 'base64').toString());
        const payload = JSON.parse(Buffer.from(payloadB64, 'base64').toString());
        const unsignedJwt = `${headerB64}.${payloadB64}`;

        if (header.alg !== 'HS256') {
            throw new Error('Invalid JWT header');
        }

        if (secret) {
            const computedSignature = btoa(crypto.subtle.digest('SHA-256', `${unsignedJwt}${secret}`));
            if (computedSignature !== signature) {
                throw new Error('Invalid JWT signature');
            }
        }

        const currentTime = Math.floor(Date.now() / 1000);
        if (payload.exp < currentTime) {
            throw new Error('JWT expired');
        }

        return payload;
    }

}