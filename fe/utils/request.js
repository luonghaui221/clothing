import axios from 'axios'

const service = axios.create({
    baseURL: 'http://localhost:8080/api',
    timeout: 50000, // request timeout
    headers: {
        'Content-Type': 'application/json',
    }
})
const IGNORED_PATHS = ['/login', '/logout', '/refresh']

service.interceptors.response.use(response => {
        return response;
    }, error => {
    const isIgnored = IGNORED_PATHS.some(path => error.config.url.includes(path))
    const statusCode = error.response ? error.response.status : -1
    if ((statusCode === 401 || statusCode === 422) && !isIgnored) {
        window.$nuxt.$store.dispatch('auth/logout')
    }
    return Promise.reject(error);
})
service.interceptors.request.use(
    config => {
        if(window.$nuxt.$store.getters['auth/isAuthenticated']){
            config.headers["Authorization"] = `Bearer ${window.$nuxt.$store.getters['auth/getAccessToken']}`;
        }
        return config;
    },
    error => {
        Promise.reject(error);
    }
)

const METHOD = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    DELETE: 'delete'
}
function isStringValid(str) {
    return typeof str === 'string' && str.trim() !== '';
}
async function request (url, method, params, config) {
    if(!url) return
    switch (method) {
        case METHOD.GET:
            return service.get(url, { params, ...config })
        case METHOD.POST:
            return service.post(url, params, config)
        case METHOD.PUT:
            return service.put(url, params, config)
        case METHOD.DELETE:
            return service.delete(url, {data: params, ...config})
        default:
            return service.get(url, { params, ...config })
    }
}
export {
    METHOD,
    request
}