
import extendRoutes from './routes/index';
import webpack from "webpack";

export default {
  publicRuntimeConfig:{
    baseUrl:process.env.BASE_URL || 'https://nuxtjs.org',
    adminBaseUrl:process.env.ADMIN_BASE_URL || 'https://nuxtjs.org',
    prefixApi:process.env.PREFIX_API,
    prefixImagesApi:process.env.PREFIX_IMAGES_API,
    urlImagesUpload: process.env.URL_IMAGES_UPLOAD
  },
  head: {
    title: 'EmpowerOutfit',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '' }
    ],
    script:[
      // {src:'js/jquery-3.3.1.min.js',body: true,defer:true},
      // {src:'js/bootstrap.min.js',body: true,defer:true},
    ]
  },

  css: [
    '~/assets/libs/material-design-icons/css/material-icons.min.css',
    '~/assets/libs/fontawesome5/css/all.css',
    '~/assets/css/bootstrap.min.css',
    '~/assets/scss/style.scss'
  ],
  router:
  {
    extendRoutes
  },
  plugins: [
    {src: '@/plugins/antd',ssr:true},
    { src: '@/plugins/vuex-persist.js', mode: 'client' },
    {src: 'plugins/vee-validate.js', ssr: false},
    {src: '~/plugins/lodash'},
    {src: '~/plugins/vue-awesome-swiper', ssr: false },
  ],
  components: true,
  buildModules: [
    '@nuxtjs/moment'
  ],
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
    '@nuxtjs/style-resources'
  ],
  styleResources: {
    scss: ['./assets/scss/style.scss']
  },

  axios: {
    // proxy: true,
    baseURL: 'http://localhost:8080'
  },
  moment: {

  },
  build: {
    vendor: ["jquery"],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    transpile: [
      'defu'
    ]
  },
  server: {
    port: 3000,
    host: process.env.BASE_URL,
  }
}
