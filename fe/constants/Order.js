export const OrderStatus = {
    PENDING: '#B7B7B7',
    READY_TO_PICK : '#1890ff',
    PICKING: '#722ed1',
    CANCELED: '#f5222d',
    PICKED: '#52c41a',
    STORING: '#a35959',
    DELIVERING: '#ffda00',
    DELIVERED: '#CDE990',
    DEFAULT: '#000000',
}