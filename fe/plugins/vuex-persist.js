import VuexPersistence from 'vuex-persist'
import Cookies from 'js-cookie'
export default ({ store }) => {
    new VuexPersistence({
        key:'auth',
        storage: {
            getItem: (key) => Cookies.get(key),
            setItem: (key, value, options) => Cookies.set(key, value, { expires:1/48 }),
            removeItem: (key) => Cookies.remove(key),
        },
        modules:['auth']
    }).plugin(store);
    new VuexPersistence({
        key:'cart',
        storage: {
            getItem: (key) => Cookies.get(key),
            setItem: (key, value, options) => Cookies.set(key, value, { expires:90 }),
            removeItem: (key) => Cookies.remove(key),
        },
        modules:['cart']
    }).plugin(store);
    new VuexPersistence({
        key:'search',
        storage: {
            getItem: (key) => Cookies.get(key),
            setItem: (key, value, options) => Cookies.set(key, value, { expires:90 }),
            removeItem: (key) => Cookies.remove(key),
        },
        modules:['search']
    }).plugin(store);
}