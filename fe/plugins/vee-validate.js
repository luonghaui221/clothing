import Vue from 'vue'

import {ValidationObserver, ValidationProvider, extend, configure} from 'vee-validate'
import {required, numeric, email, length} from 'vee-validate/dist/rules'
import {localize} from 'vee-validate';
import * as rules from 'vee-validate/dist/rules'
import Cookies from "js-cookie";

export default function ({app, $i18n}) {
  extend('required', {
    ...required
  })

  extend('select_required', {
    ...required,
    message: ""
  })

  extend('nickname_required', {
    ...required,
    message: ""
  })

  extend('password_required', {
    ...required,
    message: ""
  })

  extend('email_required', {
    ...required,
    message: ""
  })

  extend('register_code_required', {
    ...required,
    message: ""
  })

  extend('new_password_required', {
    ...required,
    message: ""
  })

  extend('appeal_required', {
    ...required,
    message: ""
  })

  extend('confirm_password', {
    params: ['target'],
    validate(value, {target}) {
      return value === target
    },
    message: ""
  })

  extend('confirm_email', {
    params: ['target'],
    validate(value, {target}) {
      return value === target
    },
    message: ""
  })

  extend('prefer_date_required', {
    ...required,
    message: ""
  })

  extend('desire_place_required', {
    ...required,
    message: ""
  })

  extend('desire_place_max', value => {
    if (value && value.length  < 4) {
      return true
    }
    return ""
  })

  extend('subject_required', {
    ...required,
    message: ""
  })

  extend('content_inquiry_required', {
    ...required,
    message: ""
  })

  extend('password_code_required', {
    ...required,
    message: ""
  })

  extend('8_password_characters', value => {
    if (value && value.length  >= 8) {
      return true
    }
    return 'validate.8_password_characters'
  })


  extend('positive', value => {
    if (value >= 0) {
      return true
    }

    return '{_field_} phải lớn hơn hoặc bằng 0'
  })

  extend('numeric', numeric)
  Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule])
  })


  extend('verify_password', {
    validate: value => {
      const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})')
      return strongRegex.test(value)
    },
    message: '{_field_} bao gồm: 1 ký tự hoa 1 ký tự thường 1 số và 1 ký tự đặc biệt: flaksfAa@123'.charAt(0).toUpperCase() + '{_field_} bao gồm: 1 ký tự hoa 1 ký tự thường 1 số và 1 ký tự đặc biệt: flaksfAa@123'.slice(1)
  })
  extend('number', {
    validate: value => {
      const strongRegex = new RegExp('^([0-9]+[,.]*)+$')
      return strongRegex.test(value)
    },
    message: 'Hãy nhập số'
  })
  extend('birthday', {
    validate: value => {
      const strongRegex = new RegExp('[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$')
      return strongRegex.test(value)
    },
    message: 'Hãy nhập đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy'
  })

  extend('default', {
    ...required,
    message: 'Mặc định là 0'
  })

  extend('phoneOrEmail', {
    validate: value => {
      const strongRegex = new RegExp(/((09|03|07|08|05)+([0-9]{8})\b)/g)
      const emailRegex = new RegExp(/\S+@\S+\.\S+/)
      return strongRegex.test(value) || emailRegex.test(value)
    },
    message: '{_field_} phải có định dạng là email hoặc số điện thoại.'
  })
  extend('Email', {
    validate: value => {
      const emailRegex = new RegExp(/\S+@\S+\.\S+/)
      return emailRegex.test(value)
    },
    message: ""
  })
  extend('Phone', {
    validate: value => {
      const strongRegex = new RegExp(/((09|03|07|08|05)+([0-9]{8})\b)/g)
      return strongRegex.test(value)
    },
    message: '{_field_} sai định dạng'
  })
  extend('Phone2', {
    validate: value => {
      const strongRegex = new RegExp(/((09|03|07|08|05|18|19)+([0-9]{8})\b)|((09|03|07|08|05|18|19)+([0-9]{6})\b)/g)
      return strongRegex.test(value)
    },
    message: '{_field_} sai định dạng'
  })
  Vue.config.productionTip = false
  Vue.component('ValidationObserver', ValidationObserver)
  Vue.component('ValidationProvider', ValidationProvider)


  localize({
    en: {
      messages: {
        select_required: 'Please select your gender.',
        required: 'Please enter {_field_}!'
      }
    },
    jp: {
      messages: {
        select_required: '性別をご選択ください。',
        required: '{_field_} ご入力ください。'
      }
    }
  });

  let LOCALE = Cookies.get('i18n_redirected');

  Object.defineProperty(Vue.prototype, "locale", {
    get() {
      return LOCALE;
    },
    set(val) {
      LOCALE = val;
      localize(val);
    }
  });


}
