import { mapActions, mapMutations } from 'vuex'

const WIDTH = 992

export default {
  watch: {
    $route(route) {
      if (this.device === 'mobile' && this.sidebar.opened) {
        this.closeSideBar({ withoutAnimation: true })
      }
    }
  },
  beforeMount() {
    window.addEventListener('resize', this.$_resizeHandler)
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.$_resizeHandler)
  },
  mounted() {
    const isMobile = this.$_isMobile()
    if (isMobile) {
      this.toggleDevice('mobile')
      this.closeSideBar({ withoutAnimation: true })
    }
  },
  methods: {
    ...mapActions('employerLayout', ['toggleDevice', 'closeSideBar','openSlideBar']),
    $_isMobile() {
      const rect = document.body.getBoundingClientRect()
      return rect.width - 1 < WIDTH
    },
    $_resizeHandler() {
      if (!document.hidden) {
        const isMobile = this.$_isMobile()
        this.$store.dispatch('employerLayout/toggleDevice', isMobile ? 'mobile' : 'desktop')

        if (isMobile) {
          this.$store.dispatch('employerLayout/closeSideBar', { withoutAnimation: true })
        }else{
          this.openSlideBar(true)
        }
      }
    }
  }
}
