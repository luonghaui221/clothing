import Cookies from "js-cookie";
import {DESIRE_PERIOD} from "@/constants/common";
const moment = require('moment')
import {mapState} from 'vuex'
import urlJoin from 'url-join'

export const commonMixin = {
  data() {
    return {
      mediaFolder: this.$config.mediaUrl,
      avatarFolder: this.$config.mediaUrl,
      optionsMediaUpload: {
        url: this.$config.mediaUrl,
        method: 'post'
      },
      domainUrl: this.$config.domainUrl,
      websocketUrl: this.$config.websocketUrl,
      femaleDefaultAvatar: require('@/assets/images/female.jpeg'),
      maleDefaultAvatar: require('@/assets/images/trend-avatar-1.jpg'),
    }
  },
  methods: {
    truncateText(str, no_words) {
      if (str !== undefined) {
        if (str.split(' ').length > no_words) {
          return str.split(' ').splice(0, no_words).join(' ') + '...'
        } else {
          return str
        }
      }
    },
    /**
     * Converts data to value: label
     */
    convertData(label, data) {
      let dataNew =[]
      if (label != '')
        dataNew = [{value: '', label: label}]
      for (let item of data) {
        dataNew.push({
          value: item.id,
          label: item.name
        })
      }
      return dataNew
    },
    /** Converts time **/
    getTime(value){
      return moment(value).locale("vi").fromNow()
    },
    getDayTime(value){
      return  moment(value).locale('vi').format('L')
    },
    convertTimeAgo(value) {
      const locale = Cookies.get('i18n_redirected')
      const local = locale === 'jp' ? 'ja': locale
      return moment(value).locale(local).fromNow()
    },
    scrollToTop() {
      window.scrollTo(0,0);
    },
    convertI18n(str) {
      return this.$i18n.t(str);
    },
    convertI18nV2(first, second) {
      return this.$i18n.t(`${first}.${second}`)
    },
    getTextDesirePeriod(val) {
      if(val === DESIRE_PERIOD.ANY_TIME) {
        return this.convertI18n('board.anytime')
      }else if(val === DESIRE_PERIOD.MORNING) {
        return this.convertI18n('board.morning')
      }else if(val === DESIRE_PERIOD.NOON) {
        return this.convertI18n('board.noon')
      }else if(val === DESIRE_PERIOD.NIGHT) {
        return this.convertI18n('board.night')
      }else if(val === DESIRE_PERIOD.EVENING) {
        return this.convertI18n('board.evening')
      }
    },
    getMediaUrl(url) {
      if(url.includes('http')){
        return url
      } else {
        return urlJoin(this.mediaFolder, url)
      }
    }
  },
  computed: {
    ...mapState(['userProfile'])
  }
}
