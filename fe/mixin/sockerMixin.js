import {mapMutations, mapState} from 'vuex'

export const socketMixin = {
    beforeMount() {
        this.initSocket()
    },
    methods: {
        ...mapMutations('roomChat', ['setMessages', 'handleMessageSocket']),
        ...mapMutations(['updateUnseenMessage']),
        initSocket() {
            var webSocketURI = this.$config.websocketUrl
            const _this = this
            var socket = new WebSocket(webSocketURI)
            this.socket = socket

            socket.onopen = function () {
            }

            socket.onclose = function (event) {
                setTimeout(function () {
                    _this.socket = _this.initSocket()
                }, 1000)
                if (event.wasClean) {
                } else {
                }
                //console.log('Code: ' + event.code + '. Reason: ' + event.reason)
            }

            socket.onmessage = function (event) {
                let message = event.data

                if (message.includes('::connected')) {
                    _this.handleUserOnline(message)
                }else if(message.includes('unseen::')) {
                    console.log('unseen');
                }

                try{
                    let msgDump = JSON.parse(message)
                    let {t, ua, ca, r, to} = msgDump
                    if(t === 'chat') {
                        _this.updateUnseenMessage({
                            to: to
                        })
                        _this.handleMessageSocket(msgDump)
                    }
                }catch (e) {
                    console.log(e)
                }
            }
            socket.onerror = function (error) {
                console.log('Error: ' + error.message)
            }
            return socket
        },
        handleUserOnline(msg) {
            let encoded = this.b64EncodeUnicode(`connect::${this.userProfile.id}`)
            this.socket.send(encoded)
        },
        b64EncodeUnicode(str) {
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                (match, p1) => {
                    return String.fromCharCode(('0x' + p1))
                }))
        },
    }
}
