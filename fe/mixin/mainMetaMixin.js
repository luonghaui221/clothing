import { mapState } from 'vuex'

export const mainMetaMixin = {
  data() {
    return{
      mediaFolder: this.$config.mediaUrl
    }

  },
  computed: {
    ...mapState(['generalWebSetting'])
  },
  head() {
    return {
      title: this.generalWebSetting.title,
      meta: [
        { property: 'og:title', content: this.generalWebSetting.title },
        { property: 'og:description', content: this.generalWebSetting.description },
        { property: 'description', content: this.generalWebSetting.description },
        { property: 'keywords', content: this.generalWebSetting.keywords },
        { property: 'og:url', content: this.domainUrl },
        { property: 'og:image', content: this.getMediaUrl(this.generalWebSetting.image) },
        { property: 'og:site_name', content: this.generalWebSetting.title },
        { property: 'og:image:width', content: '300' },
        { property: 'og:image:height', content: '300' },
      ]
    }
  },
  methods: {
    getMediaUrl(url) {
      if(url.includes('http')){
        return url
      } else {
        return `${this.mediaFolder}/${url}`
      }
    }
  }
}
