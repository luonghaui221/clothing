import axios from 'axios'
import {mapActions, mapGetters, mapMutations} from 'vuex'

export const loginMixin = {

  computed:{
    ...mapGetters({
      registerModal: 'modal/isOpen',
      loginModal: 'modal/loginModalOpen',
      modalType: 'modal/modalType'
    }),
    ...mapGetters(['userProfile','employerUserProfile']),
    currentUser() {
      return this.userProfile;
    },
    isLogin() {
      return this.currentUser && this.currentUser.id;
    }
  },
  methods: {
    ...mapMutations({
      openModal: 'modal/openModal',
      closeModal: 'modal/closeModal',
      toggleLoginModal: 'modal/toggleLoginModal',
      setModalType: 'modal/setModalType'
    }),
    openRegisterModal() {
      this.setModalType('register')
      this.openModal()
    },
    closeRegisterModal() {
      this.closeModal()
    },
    openLoginModal() {
      this.setModalType('login')
      this.openModal()
    },
    showLogin() {
      this.setModalType('login')
    },
    showRegister() {
      this.setModalType('register')
    },
    showForgotPassword() {
      this.setModalType('forgot_password')
    },
    onRegisterSuccess() {
      this.setModalType('activeAccount')
    },
    onActiveAccountSuccess() {
      this.closeRegisterModal()
    },
    changePassword() {
      this.modalType = 'change_password'
    }
  }
}
