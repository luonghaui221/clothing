import {MESSAGES} from '@/constants/messages'

export const messageBoxMixin = {
  methods: {
    handleShowNotification(result) {
      let status = ''
      let icon = ''
      if (result.isSuccess) {
        status = 'success'
        icon = "<i class='bx bx-select-multiple' ></i>"
      }
      if (result.isSuccess) {
        this.openNotification('top-right', status, icon, `<span style='color: #fff'>Thành công</span>`, MESSAGES[result.message])
      } else {
        this.openNotification('top-right', 'danger', "<i class='bx bxs-bug' ></i>", `<span style='color: #fff'>Thất bại</span>`, MESSAGES[result.message])
      }
    },
    handleShowNotificationV2(result) {
      let icon = "<i class='bx bx-select-multiple' ></i>"
      let title = this.$i18n.t('message_box.success-title')
      let message = this.$i18n.t(`message_box.${result.messageCode}`)
      if(result.status === 1) {
        this.openNotification('top-right', 'success', icon, `<span style='color: #fff'>${title}</span>`, message)
      }else {
        icon = "<i class='bx bxs-bug' ></i>";
        title = this.$i18n.t('message_box.error-title')
        this.openNotification('top-right', 'danger', icon, `<span style='color: #fff'>${title}</span>`, message)
      }
    },
    openNotification(position = null, color, icon, title, content) {
      this.$vs.notification({
        icon,
        color,
        position,
        title: title,
        text: content
      })
    },
  }
}
