import {mapMutations} from 'vuex'

export const pageTitleMixin = {
  data() {
    return {
      pageTitle: ''
    }
  },
  mounted() {
    this.updatePageTitle(this.pageTitle)
  },
  methods: {
    ...mapMutations('pageTitle', ['updatePageTitle'])
  }
}
