import {METHOD, request} from "../utils/request"

export async function uploadImages (files) {
    return (await request(`/images/upload`, METHOD.POST, files,{headers: { "Content-Type": "multipart/form-data" }}))?.data
}