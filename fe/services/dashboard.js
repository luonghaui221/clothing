import {METHOD,request} from "../utils/request";

export async function getRevenue (parameter) {
    return (await request(`/dashboard/revenue`, METHOD.GET,parameter))?.data
}
export async function getProductOutOfStock (parameter) {
    return (await request(`/dashboard/outOfStock`, METHOD.GET,parameter))?.data
}