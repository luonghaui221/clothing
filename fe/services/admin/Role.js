import {METHOD,request} from "../../utils/request";

export async function getListRole (parameter) {
    return (await request(`/role`, METHOD.GET, parameter))?.data
}