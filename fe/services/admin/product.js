import {METHOD,request} from "../../utils/request";

export async function getListProduct (parameter) {
    return (await request(`/products`, METHOD.GET, parameter))?.data
}
export async function getProductById (id,parameter) {
    return (await request(`/products/${id}`, METHOD.GET, parameter))?.data
}
export async function createProduct (parameter) {
    return (await request(`/products`, METHOD.POST, parameter))?.data
}
export async function updateProduct (id,parameter) {
    return (await request(`/products/${id}`, METHOD.PUT, parameter))?.data
}
export async function deleteProduct (id,parameter) {
    return (await request(`/products/${id}`, METHOD.DELETE, parameter))?.data
}
export async function getRelatedProduct (id,parameter) {
    return (await request(`/products/recommendations/${id}`, METHOD.GET, parameter))?.data
}