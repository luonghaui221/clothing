import {METHOD,request} from "../utils/request";

export async function registerAccount (payload) {
    return (await request(`/account/register`, METHOD.POST, payload))?.data
}
export async function login (payload) {
    return (await request(`/login`, METHOD.POST, payload))?.data
}

export async function getListAccount (payload) {
    return (await request(`/account`, METHOD.GET, payload))?.data
}

export async function lockedAccount (id, payload) {
    return (await request(`/account/${id}`, METHOD.DELETE, payload))?.data
}
export async function getAccountById (id) {
    return (await request(`/account/${id}`, METHOD.GET))?.data
}

export async function updateAccount (id,parameters) {
    return (await request(`/account/${id}`, METHOD.PUT,parameters))?.data
}