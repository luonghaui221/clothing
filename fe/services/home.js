import {METHOD,request} from "../utils/request";

export async function getListProduct (parameter) {
    return (await request(`/products`, METHOD.GET, parameter)).data
}