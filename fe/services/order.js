import {METHOD,request} from "../utils/request";

export async function createOrder (payload) {
    return (await request(`/order`, METHOD.POST, payload))?.data
}
export async function getOrderById (id) {
    return (await request(`/order/${id}`, METHOD.GET))?.data
}
export async function getMyOrder (payload) {
    return (await request(`/order`, METHOD.GET, payload))?.data
}
export async function getListOrder (payload) {
    return (await request(`/order/list`, METHOD.GET, payload))?.data
}
export async function updateOrder (id,payload) {
    return (await request(`/order/${id}`, METHOD.PUT, payload))?.data
}
export async function confirmOrder (id,payload) {
    return (await request(`/order/confirm/${id}`, METHOD.PUT, payload))?.data
}
export async function cancelOrder (id,payload) {
    return (await request(`/order/cancel/${id}`, METHOD.PUT, payload))?.data
}