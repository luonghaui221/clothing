import {METHOD,request} from "../utils/request";

export async function addToCart (payload) {
    return (await request(`/cart`, METHOD.POST, payload)).data
}
export async function addAllToCart (payload) {
    return (await request(`/cart/all`, METHOD.POST, payload)).data
}
export async function getListCartItem (payload) {
    return (await request(`/cart`, METHOD.GET, payload)).data
}
export async function updateQuantity (payload) {
    return (await request(`/cart`, METHOD.PUT, payload)).data
}
export async function removeFromCart (payload) {
    return (await request(`/cart`, METHOD.DELETE, payload)).data
}