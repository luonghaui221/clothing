import {METHOD,request} from "../utils/request";

export async function getListSize (parameter) {
    return (await request(`/size/`, METHOD.GET,parameter))?.data
}
