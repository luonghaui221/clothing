import {METHOD,request} from "../utils/request";

export async function getProductDetailsById (parameter) {
    return (await request(`/products/${parameter}`, METHOD.GET))?.data
}
export async function getProductRelated (id, parameter) {
    return (await request(`/products/recommendations/${id}`, METHOD.GET,parameter))?.data
}
export async function getProductReview (id, parameter) {
    return (await request(`/review/${id}`, METHOD.GET,parameter))?.data
}
export async function createReview (parameter) {
    return (await request(`/review`, METHOD.POST, parameter))?.data
}
export async function getAllInIds (ids) {
    return (await request(`/products/ids`, METHOD.GET,ids))?.data
}