import {METHOD,request} from "../utils/request";

export async function getListCity (parameter) {
    return (await request(`/address/province`, METHOD.GET,parameter))?.data
}
export async function getListCommune (parameter) {
    return (await request(`/address/commune`, METHOD.GET,parameter))?.data
}
export async function getListDistrict (parameter) {
    return (await request(`/address/district`, METHOD.GET,parameter))?.data
}