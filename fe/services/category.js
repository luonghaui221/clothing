import {METHOD,request} from "../utils/request";

export async function getListCategory (parameter) {
    return (await request(`/category/`, METHOD.GET,parameter))?.data
}

export async function getCategories (parameter) {
    return (await request(`/category/list`, METHOD.GET,parameter))?.data
}

export async function getCategoryById (id,parameter) {
    return (await request(`/category/${id}`, METHOD.GET,parameter))?.data
}
export async function createCategory (parameter) {
    return (await request(`/category`, METHOD.POST,parameter))?.data
}
export async function updateCategory (id, parameter) {
    return (await request(`/category/${id}`, METHOD.PUT,parameter))?.data
}
export async function deleteCategory (id,parameter) {
    return (await request(`/category/${id}`, METHOD.DELETE,parameter))?.data
}