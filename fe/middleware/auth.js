const cookieParser = require('cookieparser')
import {JWT} from "../utils/jwtUtils";
export default function ({ store, redirect, req}) {
  try{
    let token = undefined
    if (req?.headers?.cookie) {
      const parsed = cookieParser.parse(req.headers.cookie)
      const cookie = JSON.parse(JSON.stringify((parsed)))
      if(!cookie.auth) {
        throw new Error("UnAuthentication!")
      }else{
        const auth = JSON.parse(cookie.auth).auth
        token = auth.access_token
      }
    }else{
      token = store.getters['auth/getAccessToken']
    }
    const data = JWT.validateJwt(token)
    if (!data?.role?.find((s) => s === "admin")) {
      if (!data?.role?.find((s) => s === "employee")) {
        redirect('/error_page')
      } else if(path.includes('admin/order')){
        console.log('redirect employee')
        redirect('/admin/order')
      } else{
        redirect('/admin/order')
      }
    }
  }catch (err){
    redirect('/')
  }
}
